<?php
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Migration_Create_table_photo extends CI_Migration {

    public function __construct()
    {
        $this->load->dbforge();
        $this->load->database();
    }

    public function up() {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => 160,
            ],
            'path' => [
                'type' => 'VARCHAR',
                'constraint' => 180
            ],
            'album' => [
                'type' => 'VARCHAR',
                'constraint' => 20
            ],
            'is_private' => [
                'type' => 'VARCHAR',
                'constraint' => 2,
                'null' => TRUE
            ],
            'owner' => [
                'type' => 'VARCHAR',
                'constraint' => 11
            ],
            'size' => [
                'type' => 'VARCHAR',
                'constraint' => 10
            ],
            'created_at' => [
                'type' => 'TIMESTAMP'
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('photo');
    }

    public function down() {
        $this->dbforge->drop_table('photo');
    }

}
/* End of file 20190119014904_create_table_photo.php */
/* Location: ./application/migration/20190119014904_create_table_photo.php */