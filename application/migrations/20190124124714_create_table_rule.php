<?php
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Migration_Create_table_rule extends CI_Migration {

    public function __construct()
    {
        $this->load->dbforge();
        $this->load->database();
    }

    public function up() {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ],
            'menu' => [
                'type' => 'VARCHAR',
                'constraint' => 3,
            ],
            'group' => [
                'type' => 'VARCHAR',
                'constraint' => 3
            ],
            'created_at' => [
                'type' => 'TIMESTAMP'
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => TRUE
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('rule');
    }

    public function down() {
        $this->dbforge->drop_table('rule');
    }

}
/* End of file 20190124124714_create_table_rule.php */
/* Location: ./application/migration/20190124124714_create_table_rule.php */