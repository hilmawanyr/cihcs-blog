<?php
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Migration_Create_table_group extends CI_Migration {

    public function __construct()
    {
        $this->load->dbforge();
        $this->load->database();
    }

    public function up() {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => '20'
            ],
            'created_at' => [
                'type' => 'DATETIME'
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => TRUE
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('group');
    }

    public function down() {
        $this->dbforge->drop_table('group');
    }

}
/* End of file 20190113225513_create_table_group.php */
/* Location: ./application/migration/20190113225513_create_table_group.php */