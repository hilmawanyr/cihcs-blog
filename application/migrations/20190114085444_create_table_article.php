<?php
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Migration_Create_table_article extends CI_Migration {

    public function __construct()
    {
        $this->load->dbforge();
        $this->load->database();
    }

    public function up() {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ],
            'title' => [
                'type' => 'VARCHAR',
                'constraint' => 120
            ],
            'synopsys' => [
                'type' => 'TEXT'
            ],
            'content' => [
                'type' => 'LONGTEXT'
            ],
            'category' => [
                'type' => 'INT',
                'constraint' => 11
            ],
            'created_by' => [
                'type' => 'INT',
                'constraint' => 11
            ],
            'is_publish' => [
                'type' => 'INT',
                'constraint' => 1,
                'null' => TRUE
            ],
            'created_at' => [
                'type' => 'DATETIME'
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => TRUE
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('article');
    }

    public function down() {
        $this->dbforge->drop_table('article');
    }

}
/* End of file 20190114085444_create_table_article.php */
/* Location: ./application/migration/20190114085444_create_table_article.php */