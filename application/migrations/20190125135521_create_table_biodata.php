<?php
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Migration_Create_table_biodata extends CI_Migration {

    public function __construct()
    {
        $this->load->dbforge();
        $this->load->database();
    }

    public function up() {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ],
            'uid' => [
                'type' => 'VARCHAR',
                'constraint' => 12
            ],
            'birthday' => [
                'type' => 'DATE',
                'not_null' => FALSE
            ],
            'address' => [
                'type' => 'TEXT',
                'not_null' => FALSE
            ],
            'hometown' => [
                'type' => 'VARCHAR',
                'constraint' => 120
            ],
            'primary_school' => [
                'type' => 'VARCHAR',
                'constraint' => 120,
                'not_null' => FALSE
            ],
            'junior_hi_school' => [
                'type' => 'VARCHAR',
                'constraint' => 120,
                'not_null' => FALSE
            ],
            'hi_school' => [
                'type' => 'VARCHAR',
                'constraint' => 120,
                'not_null' => FALSE
            ],
            'department' => [
                'type' => 'VARCHAR',
                'constraint' => 90,
                'not_null' => FALSE
            ],
            'interest' => [
                'type' => 'TEXT',
                'not_null' => FALSE
            ],
            'about' => [
                'type' => 'VARCHAR',
                'constraint' => 360,
                'not_null' => FALSE
            ],
            'phone' => [
                'type' => 'VARCHAR',
                'constraint' => 14,
                'not_null' => FALSE
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('biodata');
    }

    public function down() {
        $this->dbforge->drop_table('biodata');
    }

}
/* End of file 20190125135521_create_table_biodata.php */
/* Location: ./application/migration/20190125135521_create_table_biodata.php */