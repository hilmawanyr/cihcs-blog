<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_table_login extends CI_Migration {

    public function __construct()
    {
        $this->load->dbforge();
        $this->load->database();
    }

    public function up() 
    {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'uid' => [
                'type' => 'VARCHAR',
                'constraint' => 11
            ],
            'username' => [
                'type' => 'VARCHAR',
                'constraint' => 80
            ],
            'password' => [
                'type' => 'VARCHAR',
                'constraint' => 40
            ],
            'group' => [
                'type' => 'INT',
                'constraint' => '2'
            ],
            'level' => [
                'type' => 'INT',
                'constraint' => 2
            ],
            'is_active' => [
                'type' => 'INT',
                'constraint' => 1
            ],
            'created_at' => [
                'type' => 'DATETIME'
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => TRUE
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('login');
    }

    public function down() {
        $this->dbforge->drop_table('login');
    }

}