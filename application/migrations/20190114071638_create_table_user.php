<?php
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Migration_Create_table_user extends CI_Migration {

    public function __construct()
    {
        $this->load->dbforge();
        $this->load->database();
    }

    public function up() {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ],
            'uid' => [
                'type' => 'VARCHAR',
                'constraint' => 11
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => 80
            ],
            'phone' => [
                'type' => 'VARCHAR',
                'constraint' => 14,
                'null' => TRUE
            ],
            'email' => [
                'type' => 'VARCHAR',
                'constraint' => 80,
                'null' => TRUE
            ],
            'avatar' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null' => TRUE
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('user');
    }

    public function down() {
        $this->dbforge->drop_table('user');
    }

}
/* End of file 20190114071638_create_table_user.php */
/* Location: ./application/migration/20190114071638_create_table_user.php */