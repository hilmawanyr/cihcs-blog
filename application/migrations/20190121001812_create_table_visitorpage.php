<?php
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Migration_Create_table_visitorpage extends CI_Migration {

    public function __construct()
    {
        $this->load->dbforge();
        $this->load->database();
    }

    public function up() {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => 50,
            ],
            'content' => [
                'type' => 'LONGTEXT',,
                'null' => TRUE
            ],
            'created_at' => [
                'type' => 'TIMESTAMP'
            ],
            'updated_at' => [
                'type' => 'DATETIME'
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('visitorpage');
    }

    public function down() {
        $this->dbforge->drop_table('visitorpage');
    }

}
/* End of file 20190121001812_create_table_visitorpage.php */
/* Location: ./application/migration/20190121001812_create_table_visitorpage.php */