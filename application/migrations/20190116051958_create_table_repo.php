<?php
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Migration_Create_table_repo extends CI_Migration {

    public function __construct()
    {
        $this->load->dbforge();
        $this->load->database();
    }

    public function up() {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ],
            'code' => [
                'type' => 'VARCHAR',
                'constraint' => 16,
                'null' => FALSE
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => 80
            ],
            'owner' => [
                'type' => 'VARCHAR',
                'constraint' => 20
            ],
            'is_private' => [
                'type' => 'VARCHAR',
                'constraint' => 1,
                'null' => TRUE
            ],
            'created_at' => [
                'type' => 'DATETIME'
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => TRUE
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('repository');
    }

    public function down() {
        $this->dbforge->drop_table('repository');
    }

}
/* End of file 20190116051958_create_table_repo.php */
/* Location: ./application/migration/20190116051958_create_table_repo.php */