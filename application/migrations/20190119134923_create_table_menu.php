<?php
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Migration_Create_table_menu extends CI_Migration {

    public function __construct()
    {
        $this->load->dbforge();
        $this->load->database();
    }

    public function up() {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => 50,
            ],
            'url' => [
                'type' => 'VARCHAR',
                'constraint' => 60
            ],
            'location' => [
                'type' => 'VARCHAR',
                'constraint' => 2
            ],
            'parent' => [
                'type' => 'VARCHAR',
                'constraint' => 3
            ],
            'icon' => [
                'type' => 'VARCHAR',
                'constraint' => 20,
                'null' => TRUE
            ],
            'created_at' => [
                'type' => 'TIMESTAMP'
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => TRUE
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('menu');
    }

    public function down() {
        $this->dbforge->drop_table('menu');
    }

}
/* End of file 20190119134923_create_table_menu.php */
/* Location: ./application/migration/20190119134923_create_table_menu.php */