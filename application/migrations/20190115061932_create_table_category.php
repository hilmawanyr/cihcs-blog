<?php
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Migration_Create_table_category extends CI_Migration {

    public function __construct()
    {
        $this->load->dbforge();
        $this->load->database();
    }

    public function up() {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => 80,
            ],
            'created_at' => [
                'type' => 'DATETIME'
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => TRUE
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('category');
    }

    public function down() {
        $this->dbforge->drop_table('category');
    }

}
/* End of file 20190115061932_create_table_category.php */
/* Location: ./application/migration/20190115061932_create_table_category.php */