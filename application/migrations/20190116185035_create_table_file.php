<?php
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Migration_Create_table_file extends CI_Migration {

    public function __construct()
    {
        $this->load->dbforge();
        $this->load->database();
    }

    public function up() {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => 90
            ],
            'path' => [
                'type' => 'VARCHAR',
                'constraint' => 120,
            ],
            'repo' => [
                'type' => 'VARCHAR',
                'constraint' => 20
            ],
            'category' => [
                'type' => 'INT',
                'constraint' => 11
            ],
            'owner' => [
                'type' => 'VARCHAR',
                'constraint' => 11
            ],
            'size' => [
                'type' => 'VARCHAR',
                'constraint' => 20,
                'null' => TRUE
            ],
            'is_private' => [
                'type' => 'VARCHAR',
                'constraint' => 2,
                'null' => TRUE
            ],
            'created_at' => [
                'type' => 'DATETIME',
                'null' => FALSE
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => TRUE
            ],
            'deleted_at' => [
                'type' => 'DATETIME',
                'null' => TRUE
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('file');
    }

    public function down() {
        $this->dbforge->drop_table('file');
    }

}
/* End of file 20190116185035_create_table_file.php */
/* Location: ./application/migration/20190116185035_create_table_file.php */