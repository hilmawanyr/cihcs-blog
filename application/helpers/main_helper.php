<?php

// get asset path
function asset($path)
{
	return base_url('assets/'.$path);
}

// generate array form to variable
function PopulateForm()
{
	$CI = &get_instance();
	$post = array();

	foreach(array_keys($_POST) as $key){
		$post[$key] = $CI->input->post($key);
	}

	return $post;
}

function getName($uid)
{
	$CI =& get_instance();
	$name = $CI->crud_model->getDetail('user','uid',$uid)->row()->name;
	return $name;
}

function categoryName($id)
{
	$CI =& get_instance();
	$category = $CI->crud_model->getDetail('category','id',$id)->row()->name;
	return $category;
}

function groupName($id)
{
	$CI =& get_instance();
	$group = $CI->crud_model->getDetail('group','id',$id)->row()->name;
	return $group;
}

function levelName(int $id) : string{
	$CI =& get_instance();
	$level = $CI->crud_model->getDetail('level','id',$id)->row()->name;
	return $level;
}

function articleStatus($status)
{
	if (is_null($status)) {
		$stats = 'not yet published';
	} elseif ($status == '1') {
		$stats = 'published';
	} elseif ($status == '2') {
		$stats = 'recheck';
	}
	return $stats;
}

function userStatus(int $id) : string
{
	$CI =& get_instance();
	if ($id == 1) {
		$status = "Active";
	} else {
		$status = "Inactive";
	}
	return $status;
}

function makeMenu(int $group) : string
{
	$list = '';
	$CI =& get_instance();
	$CI->load->model('menu/menu_model','menu');
	$parent = $CI->menu->getMenu((int)0, (int)$group);
	foreach ($parent as $parents) {
		if ($parents->url == '#') {
			$collect = '<li class="treeview">';
			$collect .= '<a href="#">';
			$collect .= '<i class="'.$parents->icon.'"></i> <span>'.$parents->name.'</span>';
			$collect .= '<span class="pull-right-container">';
			$collect .= '<i class="fa fa-angle-left pull-right"></i>';
			$collect .= '</span>';
			$collect .= '</a>';
			$collect .= '<ul class="treeview-menu">';

			$dataChild = ['parent' => $parents->id, 'location' => 1];
			$child = $CI->menu->getMenu((int)$parents->id,$group);
			foreach ($child as $childs) {
				
				$collect .= '<li><a href="'.$childs->url.'">';
				$collect .= '<i class="fa fa-circle-o"></i> '.$childs->name.'</a></li>';
				
			}
			$collect .= '</ul>';

		} else {
			$collect = '<li>';
			$collect .= '<a href="'.$parents->url.'"><i class="'.$parents->icon.'"></i> ';
			$collect .= '<span>'.$parents->name.'</span></a>';
		}

		$collect .= '</li>';
		$list = $collect.$list;
	}
	return $list;

}

function visitorMenu() : string
{
	$CI =& get_instance();
	$where = ['location' => 2, 'parent' => 0];
	$visit = $CI->crud_model->moreWhere('menu',$where,'id','desc')->result();
	$list = '';
	foreach ($visit as $key) {
		$collect = '<li class="messages-menu"><a href="'.$key->url.'">'.$key->name.'</a></li>';
		$list = $collect.$list;
	}
	return $list;
}

function catchName($code,$what)
{
	$CI =& get_instance();
	$reponame = $CI->crud_model->getDetail($what,'code',$code)->row()->name;
	return $reponame;
}

function time_elapsed_string($datetime, $full = false) 
{
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function thumbnail($code, $sort, $lim) : array
{
	$CI =& get_instance();
	$pic = $CI->crud_model->getDetail('photo','album',$code,'id',$sort,$lim)->result();
	return $pic;
}

function getMenuName($id)
{
	$CI =& get_instance();
	$name = $CI->crud_model->getDetail('menu','id',$id)->row()->name;
	return $name;
}

function userAva($uid)
{
	$CI =& get_instance();
	$ava = $CI->crud_model->getDetail('user','uid',$uid)->row()->avatar;
	$getava = base_url().substr($ava, 2);
	return $getava;
}