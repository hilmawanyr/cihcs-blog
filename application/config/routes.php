<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'landing';

// login route
$route['login'] = 'auth/login';
$route['checkuser'] = 'auth/login/loginAttemp';
$route['signout'] = 'auth/login/destroy';

// article route
$route['article/list'] = 'article';
$route['article/getdata'] = 'article/loadTable';
$route['article/newpost'] = 'article/newPost';
$route['article/post'] = 'article/post';
$route['article/(:num)/detail'] = 'article/detail/$1';
$route['article/(:num)/edit'] = 'article/loadArticle/$1';
$route['article/(:num)/update'] = 'article/update/$1';
$route['article/(:num)/destroy'] = 'article/remove/$1';
$route['article/(:num)/publish/(:num)'] = 'article/publish/$1/$2';

// category route
$route['category/list'] = 'article/category';
$route['category/store'] = 'article/category/create';
$route['category/loadedit/(:num)'] = 'article/category/beforeUpdate/$1';
$route['category/destroy/(:num)'] = 'article/category/remove/$1';

// repo soute
$route['repo/list'] = 'repository';
$route['repo/create'] = 'repository/create';
$route['repo/(:any)'] = 'repository/enter/$1';
$route['repo/edit/(:num)'] = 'repository/edit/$1';
$route['repos/change'] = 'repository/update';
$route['repos/(:num)/remove'] = 'repository/destroy/$1';

// rule menu route
$route['rule'] = 'user/rule';
$route['rule/(:num)/add'] = 'user/rule/editrule/$1';
$route['rule/store'] = 'user/rule/store';

// biodata route
$route['biodata'] = 'user/biodata';
$route['bio/store'] = 'user/biodata/store';
$route['bio/(:num)/user'] = 'user/biodata/loadBio/$1';
$route['bio/itsme'] = 'user/biodata/showme';
$route['bio/(:num)/readarticle'] = 'user/biodata/read/$1';

// group route
$route['group'] = 'user/group';
$route['group/create'] = 'user/group/create';
$route['group/edit/(:num)'] = 'user/group/edit/$1';
$route['group/(:num)/destroy'] = 'user/group/destroy/$1';

// group user
$route['user'] = 'user';
$route['user/create'] = 'user/create';
$route['user/(:num)/edit'] = 'user/edit/$1';
$route['user/(:num)/suspend'] = 'user/suspend/$1';
$route['user/(:num)/react'] = 'user/reactivate/$1';

// route level
$route['level'] = 'user/level';
$route['level/create'] = 'user/level/create';
$route['level/(:num)/edit'] = 'user/level/edit/$1';
$route['level/(:num)/remove'] = 'user/level/remove/$1';

// route album
$route['album'] = 'album';
$route['album/create'] = 'album/create';
$route['album/edit/(:num)'] = 'album/edit/$1';
$route['album/(:any)'] = 'album/enter/$1';
$route['albums/change'] = 'album/update';
$route['albums/(:num)/remove'] = 'album/remove/$1';

// visitor route
$route['visitor'] = 'visitor/page';
$route['about/store'] = 'visitor/page/storeabout';
$route['org/store'] = 'visitor/page/storeorg';
$route['contact/store'] = 'visitor/page/storecontact';
$route['edit/landing/(:num)'] = 'visitor/page/editLandingContent/$1';

// photo route
$route['photo/store'] = 'album/photo/store';
$route['photo/(:num)/type/(:any)'] = 'album/photo/changeType/$1/$2';
$route['photo/(:num)/detail'] = 'album/photo/detail/$1';
$route['photo/download/(:num)'] = 'album/photo/download/$1';
$route['photo/destroy/(:num)'] = 'album/photo/remove/$1';

// file route
$route['file/store'] = 'file/store';
$route['file/(:num)/type/(:any)'] = 'file/changeType/$1/$2';
$route['file/destroy/(:num)'] = 'file/remove/$1';
$route['file/(:num)/detail'] = 'file/detail/$1';
$route['file/download/(:num)'] = 'file/download/$1';

// notif route
$route['menu/notif'] = 'menu/notif';

// route for landing page
$route['about'] = 'landing/about';

$route['repository'] = 'landing/repository';
$route['repository/(:any)'] = 'landing/inrepos/$1';
$route['repository/(:num)/detail'] = 'landing/detail/$1/file';
$route['repository/(:num)/download'] = 'landing/download/$1/file';

$route['gallery'] = 'landing/gallery';
$route['gallery/(:any)'] = 'landing/ingallery/$1';
$route['gallery/(:num)/detail'] = 'landing/detail/$1/photo';
$route['gallery/(:num)/download'] = 'landing/download/$1/photo';

$route['contact'] = 'landing/contact';

$route['article'] = 'landing/article';
$route['article/(:num)/read'] = 'landing/read/$1';
$route['article/all'] = 'landing/allarticle';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
