<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	/**
	* @param:
	*	$tbl  table on which data will be insert
	*	$ord  key order
	* 	$typ  order type
	* @return: list of data depend on current table
	*/
	function getData(string $tbl, string $ord, string $typ)
	{
		$this->db->order_by($ord, $typ);
		return $this->db->get($tbl);
	}

	/**
	* @param:
	*	$tbl  table on which data will be insert
	*	$obj  data to be insert
	* @return: new data
	*/
	function insertData($tbl, $obj)
	{
		return $this->db->insert($tbl, $obj);
	}

	function addBatch($tbl, $obj)
	{
		return $this->db->insert_batch($tbl, $obj);
	}

	/**
	* @param:
	*	$tbl 	 table which data will be taken from it
	*	$key 	 field which be a key
	*	$val 	 value of $key
	*	$valsrt	 value for sorting data if limit will be not use, then make it empty
	*	$srt 	 sort type if limit will be not use, then make it empty
	*	$lim 	 if limit will be not use, then make it empty
	* @return one or more data retrived by search key 
	*/
	function getDetail($tbl, $key, $val, $valsrt=NULL, $srt=NULL, $lim=NULL)
	{

		$this->db->where($key, $val);

		if (!is_null($valsrt)) {
			
			$this->db->order_by($valsrt, $srt);

		}

		if (!is_null($lim)) {
			
			$this->db->limit($lim);

		}

		return $this->db->get($tbl);
	}

	/**
	* @param:
	*	$tbl 	 table which data will be taken from it
	*	$key 	 field which be a key
	*	$val 	 value of $key
	*	$typ   	 like method
	*	$valsrt	 value for sorting data if limit will be not use, then make it empty
	*	$srt 	 sort type if limit will be not use, then make it empty
	*	$lim 	 if limit will be not use, then make it empty
	* @return one or more data retrived by search key 
	*/
	function getByLike($tbl, $key, $val, $typ, $valsrt=NULL, $srt=NULL, $lim=NULL)
	{
		$this->db->like($key, $val, $typ);

		if (!is_null($valsrt)) {
			
			$this->db->order_by($valsrt, $srt);

		}

		if (!is_null($lim)) {
			
			$this->db->limit($lim);

		}

		return $this->db->get($tbl);
	}

	/**
	* @param:
	*	$tbl 	 table which data will be taken from it
	*	$arr 	 array which contain key and value of where
	*	$valsrt	 value for sorting data (if limit will be not use, then make it empty)
	*	$srt 	 sort type (if limit will be not use, then make it empty)
	*	$lim 	 if limit will be not use, then make it empty
	* @return one or more data retrived by search key 
	*/
	function moreWhere($tbl, $arr, $valsrt=NULL, $srt=NULL, $lim=NULL)
	{
		/* loop search method base on amount of "$arr" */
		for ($i=0; $i < count($arr); $i++) { 
				
			$this->db->where(array_keys($arr)[$i], array_values($arr)[$i]);

		}

		/* if you use "Sorting" method */
		if (!is_null($valsrt)) {
			
			$this->db->order_by($valsrt, $srt);

		}

		/* if you use "Limit" method */
		if (!is_null($lim)) {
			
			$this->db->limit($lim);

		}

		return $this->db->get($tbl);
	}

	/**
	* @param:
	*	$tbl 	 table which data will be taken from it
	*	$arr 	 array which contain key and value of where
	*	$valsrt	 value for sorting data (if limit will be not use, then make it empty)
	*	$srt 	 sort type (if limit will be not use, then make it empty)
	*	$lim 	 if limit will be not use, then make it empty
	* @return one or more data retrived by search key 
	*/
	function moreLike($tbl, $arr, $valsrt=NULL, $srt=NULL, $lim=NULL)
	{
		for ($i=0; $i < count($arr); $i++) { 
			
			foreach ($arr[$i] as $key => $val) {
					
				$this->db->like($key, $val, $key);

			}

		}

		/* if you use "Sorting" method */
		if (!is_null($valsrt)) {
			
			$this->db->order_by($valsrt, $srt);

		}

		/* if you use "Limit" method */
		if (!is_null($lim)) {
			
			$this->db->limit($lim);

		}

		return $this->db->get($tbl);
	}

	/**
	* @param:
	*	$tbl 	 table which data will be taken from it
	*	$key 	 array which contain key and value of where
	*	$val 	 value of $key
	* 	$obj 	 data which will be change
	* @return one or more data changed by search key 
	*/
	function updateData($tbl, $key, $val, $obj)
	{
		$this->db->where($key, $val);

		return $this->db->update($tbl, $obj);
	}

	/**
	* @param:
	*	$tbl 	 table which data will be taken from it
	*	$typ   	 use if method search is like
	*	$key 	 array which contain key and value of where
	*	$val 	 value of $key
	* 	$obj 	 data which will be change
	* @return one or more data changed by search key 
	*/
	function updateByLike($tbl, $key, $val, $obj, $typ)
	{
		$this->db->like($key, $val, $typ);

		return $this->db->update($tbl, $obj);
	}

	/**
	* @param:
	*	$tbl 	 table which data will be taken from it
	*	$arr 	 array which contain key and value of where
	*	$obj 	 data which will be change
	* @return one or more data changed by search key 
	*/
	function upMoreWhere($tbl, $arr, $obj)
	{
		/* loop search method base on amount of "$arr" */
		for ($i=0; $i < count($arr); $i++) { 
								
			$this->db->where(array_keys($arr)[$i], array_values($arr)[$i]);
			
		}

		return $this->db->update($tbl, $obj);
	}

	/**
	* @param:
	*	$tbl 	 table which data will be taken from it
	*	$arr 	 array which contain key and value of where
	*	$obj 	 data which will be change
	* @return one or more data changed by search key 
	*/
	function upMoreLike($tbl, $arr, $obj)
	{
		/* loop search method base on amount of "$arr" */
		for ($i=0; $i < count($arr); $i++) { 

			foreach ($arr[$i] as $key => $val) {
					
				$this->db->like($key, $val, $key);

			}

		}

		$this->db->update($tbl, $obj);
	}

	/**
	* @param:
	*	$tbl 	 table which data will be taken from it
	*	$key 	 array which contain key and value of where
	*	$val 	 value of $key
	* @return one or more data deleted by search key 
	*/
	function rmData($tbl, $key, $val)
	{
			
		$this->db->where($key, $val);

		return $this->db->delete($tbl);	
	}

	/**
	* @param:
	*	$tbl 	 table which data will be taken from it
	*	$typ   use if method search is like
	*	$key 	 array which contain key and value of where
	*	$val 	 value of $key
	* @return one or more data deleted by search key 
	*/
	function rmByLike($tbl, $key, $val, $typ)
	{
		$this->db->like($key, $val, $typ);

		return $this->db->delete($tbl);
	}

	/**
	* @param:
	*	$tbl 	 table which data will be taken from it
	*	$met 	 search method will be use; w = where, l = like
	*	$arr 	 array which contain key and value of where
	* @return one or more data deleted by search key 
	*/
	function rmMoreWhere($tbl, $arr)
	{
		/* loop search method base on amount of "$arr" */
		for ($i=0; $i < count($arr); $i++) { 
			
			$this->db->where(array_keys($arr)[$i], array_values($arr)[$i]);

		}

		return $this->db->delete($tbl);
	}

	/**
	* @param:
	*	$tbl 	 table which data will be taken from it
	*	$arr 	 array which contain key and value of where
	* @return one or more data deleted by search key 
	*/
	function rmMoreLike($tbl, $arr)
	{
		/* loop search method base on amount of "$arr" */
		for ($i=0; $i < count($arr); $i++) { 
							
			foreach ($arr[$i] as $key => $val) {
				
				$this->db->like($key, $val, $key);

			}

		}
	}

}

/* End of file Crud_model.php */
/* Location: ./application/models/Crud_model.php */