<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class File extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('auth')) {
			echo "<script>alert('Please re-login to start your session!')</script>";
			redirect('/login','refresh');
			exit();
		}
	}

	function store()
	{
		$user = $this->session->userdata('auth');
		$repoCode = $this->input->post('repo');
		$private = $this->input->post('isprivate');
		$category = $this->input->post('category');

		if ($_FILES['file']) {
			$this->load->helper('inflector');
			$clear	= $this->security->sanitize_filename($_FILES['file']['name']);
			$uscor	= underscore(str_replace('/', '', $clear));
			$name 	= str_replace('-', '_', $uscor);

			/**
			* if file name have more on dot (.) it will abort the upload process
			* for security reason, to prevent multiple extension
			*/
			$moreCheck = explode('.', $name);
			if (count($moreCheck) > 2) {
				echo "<script>alert('Files are only allowed to use one point in the file name!');history.go(-1);</script>";exit();
			} else {
				
				$config['allowed_types'] = 'pdf|doc|ppt|xls|docx|pptx|xlsx';
	            $config['max_size'] = '2048';
	            $config['file_name'] = $user['uid'].$repoCode.$name;
	            $config['upload_path'] = './file/';
	           
	            $this->load->library('upload', $config);

	            if (!$this->upload->do_upload('file')) {
	            	echo "<script>alert('Format or file size is not appropriate. Please repeate your upload!');history.go(-1);</script>";
	            	exit();
	                
	            } else {

	            	// insert to file
	            	$path = $config['upload_path'].$config['file_name'];
	                $data = [
						'name' => $name,
						'path' => $path,
						'repo' => $repoCode,
						'category' => $category,
						'size' => $_FILES['file']['size'],
						'owner' => $user['uid'],
						'is_private' => $private,
						'created_at'	=> date('Y-m-d H:i:s')
					];
	                $this->crud_model->insertData('file', $data);
	                echo "<script>alert('File upload successfully!');history.go(-1)</script>";
	            }
	        }
		} else {
			echo "<script>alert('There is no file uploaded!');history.go(-1);</script>";
			exit();
		}
	}

	function changeType($id,$param)
	{
		if ($param == 'c') {
			$type = NULL;
		} elseif ($param == 'e') {
			$type = 1;
		}
		$this->crud_model->updateData('file','id',$id,['is_private' => $type]);
	}

	function remove($id)
	{
		$filepath = $this->crud_model->getDetail('file','id',$id)->row()->path;
		unlink(FCPATH.substr($filepath, 1));
		$this->crud_model->rmData('file','id',$id);
	}

	function detail($id)
	{
		$filedata = $this->crud_model->getDetail('file','id',$id)->row();
		echo json_encode($filedata);
	}

	function download($id)
	{
		$getpath = $this->crud_model->getDetail('file','id',$id)->row()->path;
		$file = FCPATH.substr($getpath, 2);
		
		header('Content-Description: File Transfer');
	    header('Content-Type: application/octet-stream');
	    header('Content-Disposition: attachment; filename="'.basename($file).'"');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate');
	    header('Pragma: public');
	    header('Content-Length: ' . filesize($file));
	    readfile($file);
	    exit;
	}
}

/* End of file File.php */
/* Location: ./application/modules/file/controllers/File.php */