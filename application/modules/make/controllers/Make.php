<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Make extends CI_Controller {

    public function __construct() 
    {
        parent::__construct();

        // can only be called from the command line
        if (!$this->input->is_cli_request()) {
            exit('Direct access is not allowed. This is a command line tool, use the terminal');
        }

        $this->load->dbforge();
        $this->load->database();
    }

    public function message($to = 'World') 
    {
        echo "Hello $to!";
    }

    public function help() 
    {
        $result = "The following are the available command line interface commands\n\n";
        $result .= "php index.php tools migration \"file_name\" Create new migration file\n";
        $result .= "php index.php tools migrate [\"version_number\"] Run all migrations. The version number is optional.\n";

        echo $result . PHP_EOL;
    }

    public function migration($name) 
    {
        $this->_makeMigrationFile($name);
    }

    public function migrate($version = null) 
    {
        $this->load->library('migration');

        if ($version != null) {
            if ($this->migration->version($version) === FALSE) {
                show_error($this->migration->error_string());
            } else {
                echo "Migrations run successfully" . PHP_EOL;
            }

            return;
        }

        if ($this->migration->latest() === FALSE) {
            show_error($this->migration->error_string());
        } else {
            echo "Migrations run successfully" . PHP_EOL;
        }
    }

    protected function _makeMigrationFile($name) 
    {
        $date = new DateTime();
        $timestamp = $date->format('YmdHis');

        $table_name = strtolower($name);

        $path = APPPATH."migrations/".$timestamp."_".$name.".php";

        $my_migration = fopen($path, "w") or die("Unable to create migration file!");

        $migration_template = "<?php
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Migration_".ucfirst($name)." extends CI_Migration {

    public function __construct()
    {
        \$this->load->dbforge();
        \$this->load->database();
    }

    public function up() {
        \$this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ],
        ]);
        \$this->dbforge->add_key('id', TRUE);
        \$this->dbforge->create_table('".$table_name."');
    }

    public function down() {
        \$this->dbforge->drop_table('".$table_name."');
    }

}
/* End of file ".$timestamp."_".$name.".php */
/* Location: ./application/migration/".$timestamp."_".$name.".php */";

        fwrite($my_migration, $migration_template);

        fclose($my_migration);

        echo "Migration has successfully been created." . PHP_EOL;
    }

}