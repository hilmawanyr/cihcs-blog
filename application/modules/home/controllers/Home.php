<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('auth')) {
			echo "<script>alert('Please re-login to start your session!');history.go(-1);</script>";
			exit();
		}
	}

	public function index()
	{
		$data['page'] = "home_v";
		$this->load->view(ADMTEMPLATE, $data);
	}

}

/* End of file Home.php */
/* Location: ./application/modules/home/controllers/Home.php */