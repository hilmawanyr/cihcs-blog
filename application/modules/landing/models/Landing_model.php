<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing_model extends CI_Model {

	function ingallery($code)
	{
		$this->db->select('a.owner, a.code, p.id, p.path, p.name');
		$this->db->from('album a');
		$this->db->join('photo p', 'a.code = p.album', 'left');
		$this->db->where('a.code', $code);
		$this->db->where('p.is_private', NULL);
		return $this->db->get();
	}

	function archive($year) : array
	{
		$this->db->select('id,title');
		$this->db->from('article');
		$this->db->where('YEAR(created_at)', $year);
		return $this->db->get()->result();
	}

}

/* End of file Landing_model.php */
/* Location: ./application/modules/landing/models/Landing_model.php */