<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller {

	public function index()
	{
		$data['content'] = $this->crud_model->getDetail('visitorpage','page',28)->row();
		$data['page'] = 'landing_v';
		$this->load->view(BASETEMPLATE, $data);
	}

	public function about()
	{
		$data['about'] = $this->crud_model->getDetail('visitorpage','page',27)->row();
		$data['profile'] = $this->crud_model->getDetail('visitorpage','page',16)->row();
		$data['vision'] = $this->crud_model->getDetail('visitorpage','page',17)->row();
		$data['org'] = $this->crud_model->getDetail('visitorpage','page',18)->row();
		$data['portof'] = $this->crud_model->getDetail('visitorpage','page',19)->row();
		$data['project'] = $this->crud_model->getDetail('visitorpage','page',20)->row();
		$data['page'] = 'about_v';
		$this->load->view(BASETEMPLATE, $data);
	}

	public function repository()
	{
		$data['repo'] = $this->crud_model->getDetail('repository','is_private',NULL)->result();
		$data['page'] = 'repo_v';
		$this->load->view(BASETEMPLATE, $data);
	}

	function inrepos($code)
	{
		$where = ['repo' => $code, 'is_private' => NULL];
		$data['file'] = $this->crud_model->moreWhere('file',$where);
		$data['page'] = "listfile_v";
		$this->load->view(BASETEMPLATE, $data);
	}

	function detail($id,$what)
	{
		$search = $this->crud_model->getDetail($what,'id',$id)->row();
		echo json_encode($search);
	}

	function download($id, $what)
	{
		$getpath = $this->crud_model->getDetail($what,'id',$id)->row()->path;
		$file = FCPATH.substr($getpath, 2);
		
		header('Content-Description: File Transfer');
	    header('Content-Type: application/octet-stream');
	    header('Content-Disposition: attachment; filename="'.basename($file).'"');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate');
	    header('Pragma: public');
	    header('Content-Length: ' . filesize($file));
	    readfile($file);
	    exit;
	}

	public function gallery()
	{
		$data['gallery'] = $this->crud_model->getDetail('album','is_private',NULL)->result();
		$data['page'] = 'gallery_v';
		$this->load->view(BASETEMPLATE, $data);
	}

	function ingallery($code)
	{
		$this->load->model('landing_model', 'landing');
		$data['photo'] = $this->landing->ingallery($code);
		$data['page'] = "listphoto_v";
		$this->load->view(BASETEMPLATE, $data);
	}

	public function contact()
	{
		$data['email']  = $this->crud_model->getDetail('visitorpage','page',22)->row();
		$data['phone']  = $this->crud_model->getDetail('visitorpage','page',21)->row();
		$data['ig'] = $this->crud_model->getDetail('visitorpage','page',24)->row();
		$data['fb'] = $this->crud_model->getDetail('visitorpage','page',25)->row();
		$data['tw'] = $this->crud_model->getDetail('visitorpage','page',26)->row();
		$data['page'] = 'contact_v';
		$this->load->view(BASETEMPLATE, $data);
	}

	public function article()
	{
		$this->load->model('landing_model', 'article');
		$data['article'] = $this->crud_model->getDetail('article','is_publish',1,'id','desc',7)->result();
		$data['page'] = 'news_v';
		$this->load->view(BASETEMPLATE, $data);
	}

	function read($id)
	{
		$data['article'] = $this->crud_model->getDetail('article','id',$id)->row();
		$data['page'] = "readarticle_v";
		$this->load->view(BASETEMPLATE, $data);
	}

	function allarticle()
	{
		$data['article'] = $this->crud_model->getDetail('article','is_publish',1,'id','desc')->result();
		$data['page'] = "allarticle_v";
		$this->load->view(BASETEMPLATE, $data);
	}

}

/* End of file Landing.php */
/* Location: ./application/modules/landing/controllers/Landing.php */