<a href="javascript:void(0)" onclick="history.go(-1)" title="back" class="btn btn-warning btn-flat">
  <i class="fa fa-caret-left"></i> Back
</a>
<center>
	<h1><?= $article->title ?></h1>
  <small>by <b><?= getName($article->created_by) ?></b>, <i><?= date('M d, Y / h.i a', strtotime($article->created_at)) ?></i></small>
</center>
<div class="row" style="margin-top: 50px;">
    <div class="col-md-12">
        <div class="box-body">
          	<?= $article->content ?>
        </div>
    </div>
</div>
<p><code>#<?= categoryName($article->category) ?></code></p>