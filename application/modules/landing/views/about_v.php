<link rel="stylesheet" type="text/css" href="<?= asset('Magnific-Popup/dist/magnific-popup.css') ?>">

<style type="text/css">
  .imgstructure {
    width: 100%;
  }
</style>

<div class="row" style="margin-top: 50px;">
    <div class="col-md-6">
        <div class="box-body">
          	<h1>About Us</h1>
          	<p>
	          	<?= $about->content ?>
          	</p>
          	<br>
        </div>
    </div>

    <div class="col-md-6">
    	<div class="">
        <div class="box-body">
        	<div class="box-group" id="accordion">
            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
            <div class="panel box box-primary">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" class="collapsed">
                    Profile
                  </a>
                </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse active" aria-expanded="true" style="height: 0px;">
                <div class="box-body">
                  <?= $profile->content ?>
                </div>
              </div>
            </div>
            <div class="panel box box-primary">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed" aria-expanded="false">
                    Vision and Mission
                  </a>
                </h4>
              </div>
              <div id="collapseTwo" class="panel-collapse collapse" aria-expanded="false">
                <div class="box-body">
                  <?= $vision->content ?>
                </div>
              </div>
            </div>
            <div class="panel box box-primary">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed" aria-expanded="false">
                    Organization Structure
                  </a>
                </h4>
              </div>
              <div id="collapseThree" class="panel-collapse collapse" aria-expanded="false">
                <div class="box-body" id="box-img">
                  <a href="<?= base_url().substr($org->content, 2) ?>" class="img-str" title="organization structure">
                    <img src="<?= base_url().substr($org->content,2) ?>" class="imgstructure">
                  </a>
                </div>
              </div>
            </div>
            <div class="panel box box-primary">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="collapsed" aria-expanded="false">
                    Portofolio
                  </a>
                </h4>
              </div>
              <div id="collapseFour" class="panel-collapse collapse" aria-expanded="false">
                <div class="box-body">
                  <?= $portof->content ?>
                </div>
              </div>
            </div>
            <div class="panel box box-primary">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" class="collapsed" aria-expanded="false">
                    Ongoing Project
                  </a>
                </h4>
              </div>
              <div id="collapseFive" class="panel-collapse collapse" aria-expanded="false">
                <div class="box-body">
                  <?= $project->content ?>
                </div>
              </div>
            </div>
          </div>
        </div>
    	</div>
      <!-- /.box -->
    </div>
</div>

<script src="<?= asset('Magnific-Popup/dist/jquery.magnific-popup.js') ?>"></script>
<script type="text/javascript">
  $('#box-img').magnificPopup({
    delegate: '.img-str',
    type: 'image',
    gallery: {
      enabled:true
    },
    zoom: {
      enabled: true, // By default it's false, so don't forget to enable it
      duration: 300, // duration of the effect, in milliseconds
      easing: 'ease-in-out'
    }
  });
</script>