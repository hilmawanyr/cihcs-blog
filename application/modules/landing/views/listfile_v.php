<script type="text/javascript" src="<?= asset('custom/visitor.js') ?>"></script>
<center>
	<h1><?= catchName($file->row()->repo,'repository') ?></h1>
	<small><i>Repository of <b>#<?= trim(getName($file->row()->owner)) ?></b></i></small>
</center>
<div class="row" style="margin-top: 50px;">
  <div class="col-md-12">
      <div class="box-body">
        <!-- check format file -->
      	<?php foreach ($file->result() as $files) { ?>
          <?php $type = explode('.', $files->name);
          $format = strtolower($type[1]);
          if ($format == 'pdf') {
             $fa = 'fa fa-file-pdf-o';
           } elseif ($format == 'doc' || $format == 'docx') {
             $fa = 'fa fa-file-word-o';
           } elseif ($format == 'xls' || $format == 'xlsx') {
             $fa = 'fa fa-file-excel-o';
           } elseif ($format == 'ppt' || $format == 'pptxx') {
             $fa = 'fa fa-file-powerpoint-o';
           } ?>

      		<div class="col-md-2 col-sm-2">
            <div class="btn-group pull-right">
              <button type="button" class="btn btn-sm btn-danger btn-flat dropdown-toggle" data-toggle="dropdown">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
              </button>
              <ul class="dropdown-menu" role="menu">
                <li>
                  <a href="javascript:void(0)" onclick="detail(<?= $files->id ?>,'repository')">
                    Details
                  </a>
                </li>
                <li><a href="/repository/<?= $files->id ?>/download/">Download</a></li>
              </ul>
            </div>
      			<a href="javascript:void(0)" title="<?= $files->name ?>">
      				<i class="<?= $fa ?> fa-5x"></i>
              <label><?= substr($files->name, 0, 20) ?>...</label>
        		</a>
      		</div>
      	<?php } ?>
      </div>
  </div>
</div>

<!-- modal detail -->
<div class="modal fade" id="detailmodal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">File Details</h4>
      </div>
      <form>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <label>Name</label>
              <input class="form-control" type="text" value="" id="filename" disabled="">
            </div>
            <div class="form-group">
              <label>Type</label>
              <input class="form-control" type="text" value="" id="filetype" disabled="">
            </div>
            <div class="form-group">
              <label>Size</label>
              <input class="form-control" type="text" value="" id="filesize" disabled="">
            </div>
            <div class="form-group">
              <label>Upload date</label>
              <input class="form-control" type="text" value="" id="datecreate" disabled="">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->