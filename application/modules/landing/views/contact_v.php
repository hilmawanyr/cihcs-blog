<div class="row">
    <div class="col-md-6">
        <div class="box-body">
          	<h1>Contact Us</h1>
            <p>E-mail: <?= $email->content ?></p>
            <p>Phone: <?= $phone->content ?></p>
            <br>
            <h1>We can meet here <img src="{{ asset('img/maps.png') }}" alt="" style="width: 10%"></h1>
            <p>Jl. A.H. Nasution No.105, Cipadung, Cibiru, Kota Bandung, Jawa Barat 40614</p>
            <iframe width="450" height="250" allowfullscreen src="https://embed.waze.com/iframe?zoom=17&lat=-6.931252&lon=107.717257&ct=livemap"></iframe>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box-body">
          <h1>Find Us on</h1>
          <a class="btn btn-block btn-social btn-instagram" href="https://www.instagram.com/<?= $ig->content ?>" target="_blank">
            <i class="fa fa-instagram"></i> @<?= $ig->content ?>
          </a>
          <a class="btn btn-block btn-social btn-instagram" href="https://www.facebook.com/<?= $fb->content ?>" target="_blank">
            <i class="fa fa-facebook"></i> /<?= $fb->content ?>
          </a>
          <a class="btn btn-block btn-social btn-instagram" href="https://www.twitter.com/<?= $tw->content ?>" target="_blank">
            <i class="fa fa-twitter"></i> @<?= $tw->content ?>
          </a>
        </div>
    </div>
</div>