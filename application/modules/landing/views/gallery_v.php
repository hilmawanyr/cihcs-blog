<style type="text/css">
  .photo {
    object-fit: cover; width: 100%; height: 104px;
  }

  .thumb {
    object-fit: cover; width: 100%; height: 42px;
  }
</style>

<center>
  <h1>Gallery</h1>
  <small>#weCanJoyTogetherHere</small>
</center>

<div class="row" style="margin-top: 50px;">
  <div class="col-md-12">
    <?php foreach($gallery as $galleries) { ?>
      <div class="col-md-4 col-xs-6">
        <div class="post">
          <div class="user-block">
            <a href="/gallery/<?= $galleries->code ?>"><h3><?= $galleries->name ?></h3></a>
            <p><?= time_elapsed_string($galleries->created_at) ?></p>
          </div>

          <!-- check for picture in gallery -->
          <?php $pic = $this->crud_model->getDetail('photo','album',$galleries->code)->num_rows();
          if ($pic > 0) { ?>
            
            <div class="row margin-bottom">
              <div class="col-sm-6">
                <?php $bigthumb = thumbnail($galleries->code,'asc',1); foreach ($bigthumb as $val) { ?>
                  <img class="img-responsive photo" src="<?= base_url().substr($val->path,2) ?>">
                <?php } ?>
              </div>

              <div class="col-sm-6">
                <div class="row">
                  <div class="col-sm-6">
                    <?php $lithumb = thumbnail($galleries->code,'desc',2); foreach ($lithumb as $vals) { ?>
                      <img class="img-responsive thumb" src="<?= base_url().substr($vals->path,2) ?>">
                      <br>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>

          <?php } else { ?>
            <i>There's no picture in this album :(</i>
          <?php } ?>
          
        </div>
      </div>
    <?php } ?>
  </div>
</div>