<a href="/article" title="back" class="btn btn-warning btn-flat">
  <i class="fa fa-caret-left"></i> Back
</a>
<center>
	<h1>All Article</h1>
  	<small><i>See all of our articles. Sort by newest article.</i></small>
</center>
<div class="row" style="margin-top: 50px;">
    <div class="col-md-3">
        <?php foreach ($article as $articles) { ?>
        	
        	<div class="box box-primary">
		      <div class="box-header with-border">
		        <h3 class="box-title"><?= $articles->title ?></h3>
		      </div>
		      <!-- /.box-header -->
		      <div class="box-body">
		        <?= substr($articles->synopsys, 0, 100) ?>...
		        <a href="/article/<?= $articles->id ?>/read" title="read more">[read more]</a>
		      </div>

		      <div class="box-footer">
		      	<small>by <b><?= getName($articles->created_by) ?></b>, <i><?= date('M d, Y / h.i a', strtotime($articles->created_at)) ?></i></small>
		      </div>
		      <!-- /.box-body -->
		    </div>
        <?php } ?>
    </div>
</div>