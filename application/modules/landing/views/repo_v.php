<center>
	<h1>Repository</h1>
	<small><i>#youCanPullAnythingHere</i></small>
</center>
<div class="row" style="margin-top: 50px;">
    <div class="col-md-12">
        <div class="box-body">
          	<?php foreach ($repo as $repos) { ?>
          		<div class="col-md-3 col-sm-6">
          			<a href="/repository/<?= $repos->code ?>" title="">
          				<h5><?= $repos->name.'('.getName($repos->owner) ?>'s repository)</h5>
	          			<i class="fa fa-folder-o" id="<?= $i ?>" style="font-size: 160px"></i>
	          		</a>
          		</div>
          	<?php } ?>
        </div>
    </div>
</div>