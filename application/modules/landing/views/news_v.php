<div class="row">
  <div class="col-md-8">
    <h1><b>CIHCS</b>Article</h1>
    <h5>#letsUpgradeOurInformation</h5>
    <br>
    <ul class="timeline">
      <!-- timeline time label -->
      <?php $now = date('Y-m-d');
      $formatdate = date('M d, Y', strtotime($now)); ?>
      <li class="time-label">
          <span class="bg-red">
              <?= $formatdate ?>
          </span>
      </li>
      <!-- /.timeline-label -->

      <!-- timeline item -->
      <?php foreach($article as $articles) { ?>
      <li>
          <!-- timeline icon -->
          <i class="fa fa-newspaper-o bg-blue"></i>
          <div class="timeline-item">
              <span class="time">
                <i class="fa fa-clock-o"></i> 
                <?= date('M d, Y (h.i a)', strtotime($articles->created_at)) ?>
              </span>
              <h3 class="timeline-header">
                <a href="/article/<?= $articles->id ?>/read"><?= $articles->title ?></a>
              </h3>
              <div class="timeline-body">
                  <?= $articles->synopsys ?>
              </div>
              <div class="timeline-footer">
                  <a class="btn btn-primary btn-xs" href="/article/<?= $articles->id ?>/read">See detail</a>
              </div>
          </div>
      </li>
      <?php } ?>
      <!-- END timeline item -->
    </ul>
  </div>

  <div class="col-md-4">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Article Update</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <?php foreach($article as $listupdate) { ?>
          <p>
            <a href="/article/<?= $listupdate->id ?>/read" title="">
              <?= $listupdate->title ?>
              <small class="pull-right" style="color: grey"><i>by <?= getName($listupdate->created_by) ?></i></small>
              </a>
          </p>
        <?php } ?>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <i><a href="/article/all" title="see all article">See all</a></i>
      </div>
    </div>
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Archive</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <?php for($x = 2018; $x <= date('Y'); $x++) { ?>
          <ul class="sidebar-menu tree" data-widget="tree">
            <li class="treeview">
              <a href="#">
                <span><?= $x ?></span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <?php $archive = $this->article->archive($x);
                foreach ($archive as $archives) { ?>
                  <li>
                    <a href="/article/<?= $archives->id ?>/read/">
                      <i class="fa fa-circle-o"></i> <?= $archives->title ?>
                    </a>
                  </li>
                <?php } ?>
              </ul>
            </li>
          </ul>
        <?php } ?>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>