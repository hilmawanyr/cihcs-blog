<link rel="stylesheet" type="text/css" href="<?= asset('Magnific-Popup/dist/magnific-popup.css') ?>">
<script type="text/javascript" src="<?= asset('custom/visitor.js') ?>"></script>

<style type="text/css">
  .photo {
    object-fit: cover; width: 200px; height: 200px;
  }
</style>

<a href="/gallery" title="back" class="btn btn-flat btn-warning">
  <i class="fa fa-caret-left"></i> Back
</a>

<center>
  <h1><?= catchName($photo->row()->code,'album') ?></h1>
  <small><i>Album of <b>#<?= trim(getName($photo->row()->owner)) ?></b></i></small>
</center>
<div class="row" style="margin-top: 50px;">
  <div class="col-md-12">
    <div class="box-body">
      <?php foreach ($photo->result() as $photos) { ?>
        <!-- photo only will show if folder isn't empty -->
        <?php if ($photos->path) { ?>
        <div class="col-md-3 col-sm-3">
          <div class="btn-group pull-right">
            <button type="button" class="btn btn-sm btn-danger btn-flat dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
              <span class="sr-only">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu" role="menu">
              <li>
                <a href="javascript:void(0)" onclick="detail(<?= $photos->id ?>,'gallery')">
                  Details
                </a>
              </li>
              <li><a href="/gallery/<?= $photos->id ?>/download/">Download</a></li>
            </ul>
          </div>
        
          <a href="<?= base_url().substr($photos->path, 2) ?>" class="popup" title="<?= $photos->name ?>">
            <img src="<?= base_url().substr($photos->path, 2) ?>" alt="" class="photo">
            <label title="<?= $photos->name ?>"><?= substr($photos->name, 0, 25) ?>...</label>
          </a>
        </div>
        <!-- if album is empty :( -->
        <?php } else { ?>
          <div class="col-md-12 col-sm-12">
            <center>
              <i>No photos yet :(</i>
            </center>
          </div>  
        <?php } ?>
      <?php } ?>
    </div>
  </div>
</div>

<!-- modal detail -->
<div class="modal fade" id="detailmodal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">File Details</h4>
      </div>
      <form>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <label>Name</label>
              <input class="form-control" type="text" value="" id="filename" disabled="">
            </div>
            <div class="form-group">
              <label>Type</label>
              <input class="form-control" type="text" value="" id="filetype" disabled="">
            </div>
            <div class="form-group">
              <label>Size</label>
              <input class="form-control" type="text" value="" id="filesize" disabled="">
            </div>
            <div class="form-group">
              <label>Upload date</label>
              <input class="form-control" type="text" value="" id="datecreate" disabled="">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script src="<?= asset('Magnific-Popup/dist/jquery.magnific-popup.js') ?>"></script>
<script type="text/javascript">
  $('.box-body').magnificPopup({
    delegate: '.popup',
    type: 'image',
    gallery: {
      enabled:true
    },
    zoom: {
      enabled: true, // By default it's false, so don't forget to enable it
      duration: 300, // duration of the effect, in milliseconds
      easing: 'ease-in-out'
    }
  });
</script>