<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Photo extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('auth')) {
			echo "<script>alert('Please re-login to start your session')</script>";
			redirect('/login','refresh');
			exit();
		}
	}

	public function store()
	{
		$user = $this->session->userdata('auth');
		$albumcode = $this->input->post('album');
		$private = $this->input->post('isprivate');

		if ($_FILES['photo']) {
			$this->load->helper('inflector');
			$clear	= $this->security->sanitize_filename($_FILES['photo']['name']);
			$uscor	= underscore(str_replace('/', '', $clear));
			$name 	= str_replace('-', '_', $uscor);

			/**
			* if file name have more on dot (.) it will abort the upload process
			* for security reason, to prevent multiple extension
			*/
			$moreCheck = explode('.', $name);
			if (count($moreCheck) > 2) {
				echo "<script>alert('Files are only allowed to use one point in the file name!');history.go(-1);</script>";exit();
			} else {
				
				$config['allowed_types'] = 'jpg|jpeg|png';
	            $config['max_size'] = '2048';
	            $config['file_name'] = $user['uid'].$albumcode.$name;
	            $config['upload_path'] = './photos/';
	           
	            $this->load->library('upload', $config);

	            if (!$this->upload->do_upload('photo')) {
	            	echo "<script>alert('Format or file size is not appropriate. Please repeate your upload!');history.go(-1);</script>";
	            	exit();
	                
	            } else {

	            	// insert to file
	            	$path = $config['upload_path'].$config['file_name'];
	                $data = [
						'name' => $name,
						'path' => $path,
						'album' => $albumcode,
						'size' => $_FILES['photo']['size'],
						'owner' => $user['uid'],
						'is_private' => $private,
						'created_at'	=> date('Y-m-d H:i:s')
					];
	                $this->crud_model->insertData('photo', $data);
	                echo "<script>alert('Photo upload successfully!');history.go(-1)</script>";
	            }
	        }
		} else {
			echo "<script>alert('There is no photo uploaded!');history.go(-1);</script>";
			exit();
		}
	}

	function changeType($id,$param)
	{
		if ($param == 'c') {
			$type = NULL;
		} elseif ($param == 'e') {
			$type = 1;
		}
		$this->crud_model->updateData('photo','id',$id,['is_private' => $type]);
	}

	function detail($id)
	{
		$filedata = $this->crud_model->getDetail('photo','id',$id)->row();
		echo json_encode($filedata);
	}

	function download($id)
	{
		$getpath = $this->crud_model->getDetail('photo','id',$id)->row()->path;
		$file = FCPATH.substr($getpath, 2);
		
		header('Content-Description: File Transfer');
	    header('Content-Type: application/octet-stream');
	    header('Content-Disposition: attachment; filename="'.basename($file).'"');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate');
	    header('Pragma: public');
	    header('Content-Length: ' . filesize($file));
	    readfile($file);
	    exit;
	}

	function remove($id)
	{
		$filepath = $this->crud_model->getDetail('photo','id',$id)->row()->path;
		unlink(FCPATH.substr($filepath, 1));
		$this->crud_model->rmData('photo','id',$id);
	}

}

/* End of file Photo.php */
/* Location: ./application/modules/album/controllers/Photo.php */