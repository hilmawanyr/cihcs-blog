<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Album extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('auth')) {
			echo "<script>alert('Please re-login to start your session!')</script>";
			redirect('/login','refresh');
			exit();
		}
		$this->load->model('album/album_model', 'album');
	}

	public function index()
	{
		$user = $this->session->userdata('auth');

		// admin can see all albums and photos which not in private mode
		if ($user['group'] == 1) {
			$data['album'] = $this->album->notPrivateAlbum($user['uid']);
		} else {
			$data['album'] = $this->crus_model->getDetail('album','owner',$user['uid'])->result();
		}
		
		$data['page'] = "album_v";
		$this->load->view(ADMTEMPLATE, $data);
	}

	function create()
	{
		$user = $this->session->userdata('auth');
		$name = $this->input->post('name', TRUE);
		$private = $this->input->post('isprivate');
		$code = $this->_albumCode();

		$save = [
			'name' => $name,
			'code' => $code,
			'owner' => $user['uid'],
			'is_private' => $private,
			'created_at' => date('Y-m-d H:i:s')
		];
		$this->crud_model->insertData('album', $save);
	}

	protected function _albumCode()
	{
		$hash 	= NULL;
        $n 		= 9;
        $char 	= "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvqxyz0123456789";
        for ($i = 0; $i < $n; $i++) {
	        $rand  = rand(1, strlen($char));
	        $hash .= substr($char, $rand, 1);
	    }
	    $key = date('His').$hash;
	    return $key;
	}

	function enter($key)
	{
		$data['album'] = $this->crud_model->getDetail('album','code',$key)->row();
		$data['photo'] = $this->crud_model->getDetail('photo','album',$key)->result();
		$data['page'] = "inalbum_v";
		$this->load->view(ADMTEMPLATE, $data);
	}

	function edit(int $id)
	{
		$albumdata = $this->crud_model->getDetail('album','id',$id)->row();
		echo json_encode($albumdata);
	}

	function update()
	{
		$name = $_POST['albumname'];
		$id = $_POST['albumid'];
		$private = $_POST['isprivate'];

		$update = [
			'name' => $name,
			'is_private' => $private
		];

		$this->crud_model->updateData('album','id',$id,$update);
	}

	function remove($id)
	{
		$albumcode = $this->crud_model->getDetail('album','id',$id)->row()->code;
		$isitcontain = $this->crud_model->getDetail('photo','album',$albumcode);

		if ($isitcontain->num_rows() > 0) {
			// empty file folder
			foreach ($isitcontain->result() as $key) {
				$path = FCPATH.substr($key->path, 2);
				unlink($path);
			}

			// empty repo table
			$this->crud_model->rmData('photo','album',$albumcode);

			// then remove repo
			$this->crud_model->rmData('album','id',$id);
		} else {
			$this->crud_model->rmData('album','id',$id);
		}
	}

}

/* End of file Album.php */
/* Location: ./application/modules/album/controllers/Album.php */