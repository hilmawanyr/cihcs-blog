<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Album_model extends CI_Model {

	function notPrivateAlbum(int $uid) : array
	{
		$this->db->where('is_private !=', 1);
		$this->db->or_where('owner', $uid);
		return $this->db->get('album')->result();
	}	

}

/* End of file Album_model.php */
/* Location: ./application/modules/album/models/Album_model.php */