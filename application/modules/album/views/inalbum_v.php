<link rel="stylesheet" type="text/css" href="<?= asset('Magnific-Popup/dist/magnific-popup.css') ?>">
<script type="text/javascript" src="<?= asset('custom/album.js') ?>"></script>

<style type="text/css">
  .photo {
    object-fit: cover; width: 170px; height: 170px;
  }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="btn-group pull-right">
    <button type="button" class="btn btn-danger btn-flat">Action</button>
    <button type="button" class="btn btn-danger btn-flat dropdown-toggle" data-toggle="dropdown">
      <span class="caret"></span>
      <span class="sr-only">Toggle Dropdown</span>
    </button>
    <ul class="dropdown-menu" role="menu">
      <li><a href="#editrepo" data-toggle="modal" onclick="editAlbum(<?= $album->id ?>)">Edit Album</a></li>
      <li><a href="javascript:void(0)" onclick="rmAlbum(<?= $album->id ?>)">Delete Album</a></li>
    </ul>
  </div>
  <h1 id="titlerepo">
    <?= ucfirst($album->name) ?>
    <small>welcome to your album</small>&nbsp;
    <?php if ($album->is_private == 1) { ?>
      <i class="fa fa-lock" title="private album" ></i>
    <?php } else { ?>
      <i class="fa fa-globe" title="public album" ></i>
    <?php } ?>
  </h1>
</section>

<!-- Main content -->
<section class="content" id="maincontent">
  <a href="/album" class="btn bg-purple btn-flat" title="back"><i class="fa fa-chevron-left"></i> Back</a>
  &nbsp;
  <button class="btn btn-flat bg-navy" data-target="#addmodal" data-toggle="modal">
    <i class="fa fa-plus"></i> Upload Photo
  </button>
  <hr>
  <?php foreach ($photo as $photos) { ?>
    
    <div class="col-sm-3 col-md-3">
      <div class="box-body">

        <!-- info file type -->
        <div id="iconfile-<?= $photos->id ?>">
          <?php if ($photos->is_private == 1) { ?>
            <i class="fa fa-lock pull-left" title="private photo"></i>
          <?php } ?>
        </div>
        
        <div class="btn-group pull-right">
          <button type="button" class="btn btn-sm btn-danger btn-flat dropdown-toggle" data-toggle="dropdown">
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          </button>
          <ul class="dropdown-menu" role="menu">
            <li id="list-<?= $photos->id?>">
              <?php if ($photos->is_private == 1) { ?>
                <a href="javascript:void(0)" onclick="changeFileType(<?= $photos->id ?>,'c')">Change to public</a>
              <?php } else { ?>
                <a href="javascript:void(0)" onclick="changeFileType(<?= $photos->id ?>,'e')">Change to private</a>
              <?php } ?>
            </li>
            <li><a href="javascript:void(0)" onclick="rmPhoto(<?= $photos->id ?>)">Delete photo</a></li>
            <li><a href="/photo/download/<?= $photos->id ?>">Download</a></li>
            <li><a onclick="detail(<?= $photos->id ?>)" href="#detailmodal" data-toggle="modal">Details</a></li>
          </ul>
        </div>
        <!-- here its photos -->
        <a href="<?= base_url().substr($photos->path, 2) ?>" class="popup" title="<?= $photos->name ?>">
          <img src="<?= base_url().substr($photos->path, 2) ?>" alt="" class="photo">
          <label for="no" title="<?= $photos->name ?>"><?= substr($photos->name, 0,20) ?>...</label>
        </a>
      </div>
    </div>
  <?php } ?>
</section>
<!-- /.content -->

<div class="modal fade" id="addmodal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add photo</h4>
      </div>
      <form action="/photo/store" method="POST" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputFile">Upload photo</label>
              <input type="file" name="photo" id="exampleInputFile">

              <i class="help-block">file type must be .JPG, .JPEG, or .PNG (max. 2 MB)</i>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" value="1" name="isprivate"> Private photo
              </label>
            </div>
            <input type="hidden" value="<?= $album->code ?>" name="album">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Upload</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- modal detail -->
<div class="modal fade" id="detailmodal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">File Details</h4>
      </div>
      <form action="/file/store" method="POST" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <label>Name</label>
              <input class="form-control" type="text" value="" id="filename" disabled="">
            </div>
            <div class="form-group">
              <label>Type</label>
              <input class="form-control" type="text" value="" id="filetype" disabled="">
            </div>
            <div class="form-group">
              <label>Size <i>(kb)</i></label>
              <input class="form-control" type="text" value="" id="filesize" disabled="">
            </div>
            <div class="form-group">
              <label>Upload date</label>
              <input class="form-control" type="text" value="" id="datecreate" disabled="">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- rename repo -->
<div class="modal fade" id="editrepo">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Album</h4>
      </div>
      <form id="editalbumform">
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <label>Name</label>
              <input type="text" class="form-control" name="albumname" id="albumname"  value="">
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" value="1" id="albumtype" name="isprivate"> Private repository
              </label>
            </div>
            <input type="hidden" value="" name="albumid" id="albumid">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="button" onclick="updateAlbum()" class="btn btn-primary">Update</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script src="<?= asset('Magnific-Popup/dist/jquery.magnific-popup.js') ?>"></script>
<script type="text/javascript">
  $('.box-body').magnificPopup({
    delegate: '.popup',
    type: 'image',
    zoom: {
      enabled: true, // By default it's false, so don't forget to enable it
      duration: 300, // duration of the effect, in milliseconds
      easing: 'ease-in-out'
    }
  });
</script>