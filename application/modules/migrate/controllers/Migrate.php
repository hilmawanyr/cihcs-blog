<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migrate extends CI_Controller {

	public function version($v)
	{
		$this->load->library('migration');

        if ($this->migration->version($v) === FALSE)
        {
            show_error($this->migration->error_string());
        } else {
        	echo "<script>alert('Migration success!');history.go(-1);</script>";
        }
	}

}

/* End of file Migrate.php */
/* Location: ./application/modules/migrate/controllers/Migrate.php */