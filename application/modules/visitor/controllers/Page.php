<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('auth')) {
			echo "<script>alert('Please re-login to start your session!')</script>";
			redirect('/login','refresh');
			exit();
		}
	}

	public function index()
	{
		$where = ['location' => 2, 'parent !=' => 0];
		$data['data'] = $this->crud_model->moreWhere('menu',$where,'id','asc')->result();
		$data['page'] = "visitor_v";
		$this->load->view(ADMTEMPLATE, $data);
	}

	function storeabout()
	{
		$id = $this->input->post('idabout');
		$content = $this->input->post('content');

		$data = [
			'page' => $id,
			'content' => $content
		];

		$checkavailable = $this->crud_model->getDetail('visitorpage','page',$id)->num_rows();
		// if data exist, then update
		if ($checkavailable > 0) {
			$data['updated_at'] = date('Y-m-d H:i:s');
			$this->crud_model->updateData('visitorpage','page',$id,$data);
		} else {
			$data['created_at'] = date('Y-m-d H:i:s');
			$this->crud_model->insertData('visitorpage',$data);
		}
	}

	function storeorg()
	{
		$idcontent = $this->input->post('idorganize');

		if ($_FILES['organization']) {
			$this->load->helper('inflector');
			$clear	= $this->security->sanitize_filename($_FILES['organization']['name']);
			$uscor	= underscore(str_replace('/', '', $clear));
			$name 	= str_replace('-', '_', $uscor);

			/**
			* if file name have more on dot (.) it will abort the upload process
			* for security reason, to prevent multiple extension
			*/
			$moreCheck = explode('.', $name);
			if (count($moreCheck) > 2) {
				echo "<script>alert('Files are only allowed to use one point in the file name!');history.go(-1);</script>";exit();
			} else {
				
				$config['allowed_types'] = 'jpg|jpeg|png';
	            $config['max_size'] = '2048';
	            $config['file_name'] = date('His').$name;
	            $config['upload_path'] = './photos/imgstructure/';
	           
	            $this->load->library('upload', $config);

	            if (!$this->upload->do_upload('organization')) {
	            	echo "<script>alert('Format or file size is not appropriate. Please repeate your upload!');history.go(-1);</script>";
	            	exit();
	                
	            } else {

	            	// insert to file
	            	$path = $config['upload_path'].$config['file_name'];
	                $data = [
						'page' => $idcontent,
						'content' => $path,
					];

					$checkavailable = $this->crud_model->getDetail('visitorpage','page',$idcontent)->num_rows();
					// if file exist, then update
					if ($checkavailable > 0) {
						$data['updated_at'] = date('Y-m-d H:i:s');
						$this->crud_model->updateData('visitorpage','page',$idcontent,$data);
					} else {
						$data['created_at'] = date('Y-m-d H:i:s');
						$this->crud_model->insertData('visitorpage', $data);
					}
	                echo "<script>alert('Photo upload successfully!');history.go(-1)</script>";
	            }
	        }
		} else {
			echo "<script>alert('There is no photo uploaded!');history.go(-1);</script>";
			exit();
		}
		
	}

	function storecontact()
	{
		$id = $this->input->post('idcontact');
		$contact = $this->input->post('input-contact');

		$data = [
			'page' => $id,
			'content' => $contact
		];

		$checkavailable = $this->crud_model->getDetail('visitorpage','page',$id)->num_rows();
		// if data available, then update
		if ($checkavailable > 0) {
			$data['updated_at'] = date('Y-m-d H:i:s');
			$this->crud_model->updateData('visitorpage','page',$id,$data);
		} else {
			$data['created_at'] = date('Y-m-d H:i:s');
			$this->crud_model->insertData('visitorpage', $data);
		}
	}

	function editLandingContent($id)
	{
		$dataload = $this->crud_model->getDetail('visitorpage','page',$id)->row();
		echo json_encode($dataload);
	}

}

/* End of file Page.php */
/* Location: ./application/modules/visitor/controllers/Page.php */