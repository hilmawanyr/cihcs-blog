<link rel="stylesheet" type="text/css" href="<?= asset('summernote-master/dist/summernote.css') ?>">
<script type="text/javascript" src="<?= asset('summernote-master/dist/summernote.js') ?>"></script>

<script type="text/javascript" src="<?= asset('custom/visitor.js') ?>"></script>
<style type="text/css">
  .loadimg {
    width: 20%;
  }

  .colimg {
    text-align: center;
  }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Visitor page
    <small>manage static page for visitor from here</small>
  </h1>
</section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Page list</h3>
    </div>
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>No</th>
            <th>Child Page</th>
            <th>Parent Page</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1; foreach ($data as $key) { ?>
            <tr>
              <td><?= $no ?></td>
              <td><?= $key->name ?></td>
              <td><?= getMenuName($key->parent) ?></td>
              <td width="100">
                <!-- is exist ? -->
                <?php $has = $this->crud_model->getDetail('visitorpage','page',$key->id)->num_rows(); ?>
                <?php if ($has > 0) { ?>
                  <button class="btn btn-flat bg-green" onclick="editstatic(<?= $key->id.','.$key->parent.','."'".$key->name."'" ?>)"><i class="fa fa-pencil"></i>
                  </button>
                <?php } else { ?>
                  <button class="btn btn-flat bg-purple" onclick="setstatic(<?= $key->id.','.$key->parent.','."'".$key->name."'" ?>)">
                    <i class="fa fa-navicon"></i>
                  </button>
                <?php } ?>
                                
              </td>
            </tr>
          <?php $no++; } ?>
        </tbody>
        <tfoot>
          <tr>
            <th>No</th>
            <th>Child Page</th>
            <th>Parent Page</th>
            <th>Action</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
    <div class="box-footer"></div>
    <!-- /.box-footer-->
  </div>
  <!-- /.box -->
</section>
<!-- /.content -->

<div class="modal fade" id="about">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modtitle"></h4>
      </div>
      <form id="about-form">
        <div class="modal-body">
          <div class="box-body">
            <textarea id="editor1" name="content" rows="10" cols="80"><div id="here"></div></textarea>
            <input type="hidden" name="idabout" id="idabout" value="">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="button" onclick="storeabout()" class="btn btn-default bg-purple" id="saveabout">Submit</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="organize">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modstitle"></h4>
      </div>
      <form action="/org/store" method="post" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <label>Upload photo</label>
              <input type="file" name="organization">
              <br>
              <p><i>allowed type: <b>.JPG or .PNG (max. 2 MB)</b></i></p>
            </div>
            <input type="hidden" name="idorganize" id="idorganize" value="">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-default bg-purple">Upload</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="contact">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="contactitle"></h4>
      </div>
      <form id="contact-form">
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <label id="for-lable"></label>
              <input type="text" name="input-contact" id="input-contact" class="form-control">
            </div>
            <input type="hidden" name="idcontact" id="idcontact" value="">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="button" onclick="storecontact()" class="btn btn-default bg-purple" id="savecontact">Save</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script type="text/javascript">
  $('#editor1').summernote({
    height: 300,     // set editor height
    minHeight: null, // set minimum height of editor
    maxHeight: null, // set maximum height of editor
    focus: true      // set focus to editable area after initializing summernote
  });
</script>