<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('auth')) {
			redirect('login','refresh');
		}
		$this->load->model('article/article_model', 'article');
	}

	public function index()
	{
		$data['page'] = "article_v";
		$this->load->view(ADMTEMPLATE, $data);
	}

	function loadTable()
	{
		$user = $this->session->userdata('auth');
		$data['data'] = $this->article->list($user['level'],$user['uid']);
		$this->load->view('articletable_v', $data);
	}

	function detail($id)
	{
		$article = $this->article->articleDetail($id);
		echo json_encode($article);
	}

	function newPost()
	{
		$data['data'] = $this->crud_model->getData('category','name','asc')->result();
		$data['page'] = "newarticle_v";
		$this->load->view(ADMTEMPLATE, $data);
	}

	function post()
	{
		$user = $this->session->userdata('auth');
		$post = [
			'title' => $_POST['title'],
			'synopsys' => $_POST['syn'],
			'content' => $_POST['content'],
			'category' => $_POST['category'],
			'created_by' => $user['uid'],
			'created_at' => date('Y-m-d H:i:s'),
		];
		$this->crud_model->insertData('article', $post);
	}

	function loadArticle($id)
	{
		$data['catg'] = $this->crud_model->getData('category','name','asc')->result();
		$data['data'] = $this->crud_model->getDetail('article','id',$id)->row();
		$data['page'] = "articleload_v";
		$this->load->view(ADMTEMPLATE, $data);
	}

	function update($id)
	{
		$user = $this->session->userdata('auth');
		$post = [
			'title' => $_POST['title'],
			'synopsys' => $_POST['syn'],
			'content' => $_POST['content'],
			'category' => $_POST['category'],
			'created_by' => $user['uid'],
			'updated_at' => date('Y-m-d H:i:s'),
		];
		$this->crud_model->updateData('article','id',$id,$post);
	}

	function remove($id)
	{
		$this->crud_model->rmData('article','id',$id);
	}

	function publish($id,$status)
	{
		$this->crud_model->updateData('article','id',$id,['is_publish' => $status]);
	}

}

/* End of file Article.php */
/* Location: ./application/modules/article/controllers/Article.php */