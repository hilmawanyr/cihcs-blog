<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('auth')) {
			echo "<script>alert('Please re-login to start your session!')</script>";
			redirect('/login','refresh');
			exit();
		}
	}

	public function index()
	{
		$data['data'] = $this->crud_model->getData('category','name','asc')->result();
		$data['page'] = "category_v";
		$this->load->view(ADMTEMPLATE, $data);
	}

	function create()
	{
		// if action is update
		if ($_POST['saveup'] == 'update') {
			$arr = ['name' => $_POST['name'], 'updated_at' => date('Y-m-d H:i:s')];
			$id = $this->input->post('idcat', TRUE);
			$this->_update((int)$id,(array)$arr);

		// if action is create
		} else {
			$name = $_POST['name'];
			$post = [
				'name' => $name,
				'created_at' => date('Y-m-d H:i:s')
			];
			$this->crud_model->insertData('category',$post);
		}
	}

	function beforeUpdate($id)
	{
		$data = $this->crud_model->getDetail('category','id',$id)->row();
		echo json_encode($data);
	}

	protected function _update(int $id, array $arr)
	{
		$this->crud_model->updateData('category','id',$id,$arr);
	}

	function remove($id)
	{
		$this->crud_model->rmData('category','id',$id);
	}

}

/* End of file Category.php */
/* Location: ./application/modules/article/controllers/Categorie.php */