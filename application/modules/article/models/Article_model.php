<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article_model extends CI_Model {

	function list($l,$u) : array
	{
		if ($l == '3') {
			return $this->db->get('article')->result();
		} else {
			return $this->crud_model->getDetail('article','created_by',$u)->result();
		}
	}

	function articleDetail($id) : object
	{
		$this->db->select('a.*, b.name as catname');
		$this->db->from('article a');
		$this->db->join('category b', 'a.category = b.id');
		$this->db->where('a.id', $id);
		$this->db->limit(1);
		return $this->db->get()->row();
	}

}

/* End of file Article_model.php */
/* Location: ./application/modules/article/models/Article_model.php */