<link rel="stylesheet" type="text/css" href="<?= asset('summernote-master/dist/summernote.css') ?>">
<script type="text/javascript" src="<?= asset('summernote-master/dist/summernote.js') ?>"></script>

<script type="text/javascript" src="<?= asset('custom/article.js') ?>"></script>

<style type="text/css">
  .custom {
    vertical-align: middle !important;
  }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?= $data->title ?>
    <small>edit article here</small>
  </h1>
</section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title custom">Edit article</h3>
      <a href="/article/list" class="btn btn-flat btn-warning pull-right"><i class="fa fa-arrow-left"></i> Back</a>
    </div>
    <form class="form-horizontal" id="formupdate">
      <div class="box-body">

        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Title</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" value="<?= $data->title ?>" name="title">
          </div>
        </div>

        <div class="form-group">
          <label for="inputPassword3" class="col-sm-2 control-label">Synopsys</label>
          <div class="col-sm-10">
            <textarea class="form-control" name="syn"><?= $data->synopsys ?></textarea>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label">Category</label>
          <div class="col-sm-10">
            <select class="form-control" name="category">
              <option selected="" disabled=""></option>
              <?php foreach ($catg as $key) { ?>
                <option value="<?= $key->id ?>"><?= $key->name ?></option>
              <?php } ?>
            </select>
          </div>
        </div>

        <textarea id="editor1" name="content" rows="10" cols="80"><?= $data->content ?></textarea>

        <button type="button" onclick="update(<?= $key->id ?>)" class="pull-right btn btn-flat bg-purple">
          Update Article
        </button>
        
      </div>
    </form>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>
<!-- /.content -->

<script type="text/javascript">
  $('#editor1').summernote({
    height: 300,     // set editor height
    minHeight: null, // set minimum height of editor
    maxHeight: null, // set maximum height of editor
    focus: true      // set focus to editable area after initializing summernote
  });
</script>