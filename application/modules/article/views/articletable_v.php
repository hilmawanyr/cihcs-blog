<?php $sess = $this->session->userdata('auth'); ?>
<?php $no = 1; foreach ($data as $key) { ?>
	<tr>
		<td><?= $no ?></td>
		<td><?= $key->title ?></td>
		<td><?= getName($key->created_by) ?></td>
		<td><?= $key->created_at ?></td>
		<td><?= articleStatus($key->is_publish) ?></td>
		<td width="125">
			<?php if ($sess['level'] == '3') { ?>
				<button class="btn btn-flat btn-info" title="detail" onclick="detail(<?= $key->id ?>)">
					<i class="fa fa-eye"></i>
				</button>
				<button class="btn btn-flat bg-navy" title="publish" onclick="publish(<?= $key->id ?>,1)"><i class="fa fa-check"></i></button>
				<button class="btn btn-flat btn-danger" title="publish" onclick="publish(<?= $key->id ?>,2)"><i class="fa fa-times"></i></button>
			<?php } else { ?>
				
				<a class="btn bg-purple btn-flat" title="edit" href="/article/<?= $key->id ?>/edit">
					<i class="fa fa-pencil"></i>
				</a>
				<button class="btn btn-danger btn-flat" title="delete" onclick="remove(<?= $key->id ?>)"><i class="fa fa-trash"></i></button>
				<button class="btn btn-flat btn-info" title="detail" onclick="detail(<?= $key->id ?>)">
					<i class="fa fa-eye"></i>
				</button>

			<?php } ?>
		</td>
	</tr>
<?php } ?>