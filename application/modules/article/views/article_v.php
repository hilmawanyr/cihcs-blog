<script type="text/javascript" src="<?= asset('custom/article.js') ?>"></script>
<style type="text/css">
  .loadimg {
    width: 20%;
  }

  .colimg {
    text-align: center;
  }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Article
    <small>manage article here</small>
  </h1>
</section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Article list</h3>
    </div>
    <div class="box-body">
      <a class="btn btn-flat btn-success" href="/article/newpost"><i class="fa fa-plus"></i> Add article</a>
      <hr>
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>No</th>
            <th>Title</th>
            <th>Creator</th>
            <th>Create date</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody id="showlist">
          <tr id="loader">
            <td></td>
            <td></td>
            <td class="colimg"><img src="<?= asset('img/loader.gif') ?>" alt="" class="loadimg"></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
        </tbody>
        <tfoot>
          <tr>
            <th>No</th>
            <th>Title</th>
            <th>Creator</th>
            <th>Create date</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
    <div class="box-footer"></div>
    <!-- /.box-footer-->
  </div>
  <!-- /.box -->
</section>
<!-- /.content -->

<div class="modal fade" id="detail">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-titl">Article detail</h4>
      </div>
      <form id="addcategory">
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <label for="title">Title</label>
              <input type="text" class="form-control" id="title" readonly="">
            </div>
            <div class="form-group">
              <label for="cat">Category</label>
              <input type="text" class="form-control" id="cat" readonly="">
            </div>
            <div class="form-group">
              <label>Synopsys</label>
              <textarea class="form-control" rows="3" id="syn" disabled=""></textarea>
            </div>
            <div class="box box-default">
              <div class="box-body" id="content"></div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->