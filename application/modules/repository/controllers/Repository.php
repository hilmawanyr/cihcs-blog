<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Repository extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('auth')) {
			echo "<script>alert('Please re-login to start your session!')</script>";
			redirect('/login','refresh');
			exit();
		}
	}

	public function index()
	{
		$user = $this->session->userdata('auth');
		$data['repo'] = $this->crud_model->getDetail('repository','owner',$user['uid'])->result();
		$data['page'] = "repo_v";
		$this->load->view(ADMTEMPLATE, $data);
	}

	function create()
	{
		$user = $this->session->userdata('auth');
		$name = xss_clean($_POST['name']);
		$private = $_POST['isprivate'];
		$code = $this->_repoCode();

		$save = [
			'name' => $name,
			'code' => $code,
			'owner' => $user['uid'],
			'is_private' => $private,
			'created_at' => date('Y-m-d H:i:s')
		];
		$this->crud_model->insertData('repository', $save);
	}

	protected function _repoCode()
	{
		$hash 	= NULL;
        $n 		= 9;
        $char 	= "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvqxyz0123456789";
        for ($i = 0; $i < $n; $i++) {
	        $rand  = rand(1, strlen($char));
	        $hash .= substr($char, $rand, 1);
	    }
	    $key = $hash.date('ymdHis');
	    return $key;
	}

	function enter(string $id)
	{
		$user = $this->session->userdata('auth');
		$data['repo'] = $this->crud_model->getDetail('repository','code', $id)->row();
		$data['catg'] = $this->crud_model->getData('category','name','asc')->result();
		$data['file'] = $this->crud_model->moreWhere('file',['repo' => $id, 'owner' => $user['uid']])->result();
		$data['page'] = "inrepo_v";
		$this->load->view(ADMTEMPLATE, $data);
	}

	function edit(int $id)
	{
		$repodata = $this->crud_model->getDetail('repository','id',$id)->row();
		echo json_encode($repodata);
	}

	function update()
	{
		$name = $_POST['reponame'];
		$id = $_POST['repoid'];
		$private = $_POST['isprivate'];

		$update = [
			'name' => $name,
			'is_private' => $private
		];

		$this->crud_model->updateData('repository','id',$id,$update);
	}

	function destroy($id)
	{
		$repocode = $this->crud_model->getDetail('repository','id',$id)->row()->code;
		$isitcontain = $this->crud_model->getDetail('file','repo',$repocode);

		if ($isitcontain->num_rows() > 0) {
			// empty file folder
			foreach ($isitcontain->result() as $key) {
				$path = FCPATH.substr($key->path, 2);
				unlink($path);
			}

			// empty repo table
			$this->crud_model->rmData('file','repo',$repocode);

			// then remove repo
			$this->crud_model->rmData('repository','id',$id);
		} else {
			$this->crud_model->rmData('repository','id',$id);
		}
	}

}

/* End of file Repository.php */
/* Location: ./application/modules/repository/controllers/Repository.php */