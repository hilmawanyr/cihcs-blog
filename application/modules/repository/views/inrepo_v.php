<script type="text/javascript" src="<?= asset('custom/repo.js') ?>"></script>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="btn-group pull-right">
    <button type="button" class="btn btn-danger btn-flat">Action</button>
    <button type="button" class="btn btn-danger btn-flat dropdown-toggle" data-toggle="dropdown">
      <span class="caret"></span>
      <span class="sr-only">Toggle Dropdown</span>
    </button>
    <ul class="dropdown-menu" role="menu">
      <li><a href="#editrepo" data-toggle="modal" onclick="editRepo(<?= $repo->id ?>)">Edit Repository</a></li>
      <li><a href="javascript:void(0)" onclick="rmRepo(<?= $repo->id ?>)">Delete Repository</a></li>
    </ul>
  </div>
  <h1 id="titlerepo">
    <?= $repo->name ?>
    <small>welcome to your repository</small>&nbsp;
    <?php if ($repo->is_private == 1) { ?>
      <i class="fa fa-lock" title="private repository" ></i>
    <?php } else { ?>
      <i class="fa fa-globe" title="public repository" ></i>
    <?php } ?>
  </h1>
</section>

<!-- Main content -->
<section class="content" id="maincontent">
  <a href="/repo/list" class="btn bg-purple btn-flat" title="back"><i class="fa fa-chevron-left"></i> Back</a>
  &nbsp;
  <button class="btn btn-flat bg-navy" data-target="#addmodal" data-toggle="modal">
    <i class="fa fa-plus"></i> Upload File
  </button>
  <hr>
  <?php foreach ($file as $files) { ?>
    <!-- check file type -->
    <?php $format = explode('.', $files->name);
    if ($format[1] == 'pdf') {
      $fa = 'fa-file-pdf-o';
    } elseif ($format[1] == 'doc' || $format[1] == 'docx') {
      $fa = 'fa-file-word-o';
    } elseif ($format[1] == 'xls' || $format[1] == 'xlsx') {
      $fa = 'fa-file-excel-o';
    } elseif ($format[1] == 'ppt' || $format[1] == 'pptx') {
      $fa = 'fa-file-powerpoint-o';
    } ?>

    <div class="col-sm-2 col-md-2">
      <div class="box-body">

        <!-- info file type -->
        <div id="iconfile-<?= $files->id ?>">
          <?php if ($files->is_private == 1) { ?>
            <i class="fa fa-lock pull-left" title="private file"></i>
          <?php } ?>
        </div>
        
        <div class="btn-group pull-right">
          <button type="button" class="btn btn-sm btn-danger btn-flat dropdown-toggle" data-toggle="dropdown">
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          </button>
          <ul class="dropdown-menu" role="menu">
            <li id="list-<?= $files->id?>">
              <?php if ($files->is_private == 1) { ?>
                <a href="javascript:void(0)" onclick="changeFileType(<?= $files->id ?>,'c')">Change to public</a>
              <?php } else { ?>
                <a href="javascript:void(0)" onclick="changeFileType(<?= $files->id ?>,'e')">Change to private</a>
              <?php } ?>
            </li>
            <li><a href="javascript:void(0)" onclick="rmFile(<?= $files->id ?>)">Delete file</a></li>
            <li><a href="/file/download/<?= $files->id ?>">Download</a></li>
            <li><a onclick="detail(<?= $files->id ?>)" href="#detailmodal" data-toggle="modal">Details</a></li>
          </ul>
        </div>
        <a href="javascript:void(0)" title="<?= $files->name ?>">
          <i class="fa <?= $fa ?> fa-5x"></i>
          <label for="no" title="<?= $files->name ?>"><?= substr($files->name, 0,20) ?>...</label>
        </a>
        <!-- lable for file category -->
        <span class="badge bg-purple"><?= categoryName($files->category) ?></span>
      </div>
    </div>
  <?php } ?>
</section>
<!-- /.content -->

<div class="modal fade" id="addmodal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add file</h4>
      </div>
      <form action="/file/store" method="POST" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <label>Category</label>
              <select class="form-control" name="category">
                <option selected="" disabled=""></option>
                <?php foreach ($catg as $val) { ?>
                  <option value="<?= $val->id ?>"><?= $val->name ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label for="exampleInputFile">Upload file</label>
              <input type="file" name="file" id="exampleInputFile">

              <i class="help-block">file type must be .PDF, .DOC/.DOCX, .XLS/.XLSX, or .PPT/.PPTX (max. 2 MB)</i>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" value="1" name="isprivate"> Private file
              </label>
            </div>
            <input type="hidden" value="<?= $repo->code ?>" name="repo">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Upload</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- modal detail -->
<div class="modal fade" id="detailmodal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">File Details</h4>
      </div>
      <form action="/file/store" method="POST" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <label>Name</label>
              <input class="form-control" type="text" value="" id="filename" disabled="">
            </div>
            <div class="form-group">
              <label>Type</label>
              <input class="form-control" type="text" value="" id="filetype" disabled="">
            </div>
            <div class="form-group">
              <label>Size</label>
              <input class="form-control" type="text" value="" id="filesize" disabled="">
            </div>
            <div class="form-group">
              <label>Upload date</label>
              <input class="form-control" type="text" value="" id="datecreate" disabled="">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- rename repo -->
<div class="modal fade" id="editrepo">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Repository</h4>
      </div>
      <form id="editrepoform">
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <label>Name</label>
              <input type="text" class="form-control" name="reponame" id="reponame"  value="">
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" value="1" id="repotype" name="isprivate"> Private repository
              </label>
            </div>
            <input type="hidden" value="" name="repoid" id="repoid">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="button" onclick="updateRepo()" class="btn btn-primary">Update</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->