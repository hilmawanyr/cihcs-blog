<script type="text/javascript" src="<?= asset('custom/repo.js') ?>"></script>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Repository
    <small>create your repository here</small>
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <button class="btn btn-flat btn-warning" data-target="#addmodal" data-toggle="modal">
    <i class="fa fa-plus"></i> Create Repository
  </button>
  <hr>
  <?php foreach ($repo as $key) { ?>
    <div class="col-sm-2">
      <a href="/repo/<?= $key->code ?>" title="">
        <?php if (!is_null($key->is_private)) { ?>
          <i class="fa fa-lock"></i>
        <?php } ?>
        <i id="no" class="fa fa-folder-open-o fa-5x"></i><br>
        <label for="no"><?= $key->name ?></label>
      </a>
    </div>
  <?php } ?>
</section>
<!-- /.content -->

<div class="modal fade" id="addmodal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add repository</h4>
      </div>
      <form id="addrepo">
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <label for="title">Name</label>
              <input type="text" class="form-control" id="title" name="name">
            </div>
            <div class="col-sm-10">
              <div class="checkbox">
                <label>
                  <input type="checkbox" value="1" name="isprivate"> Private repository
                </label>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="button" onclick="create()" class="btn btn-primary">Save repository</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->