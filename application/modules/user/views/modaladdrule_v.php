<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title" id="modal-titl">Add rule</h4>
</div>
<form id="addrules">
  <div class="modal-body">
    <div class="box-body">
      <table class="table">
        <tr>
          <td>No</td>
          <td>Menu</td>
          <td>Choose</td>
        </tr>
        <?php $no = 1; foreach ($menuparent as $key) : ?>
          <!-- check if rule was exist -->
          <?php $where = ['menu' => $key->id, 'group' => $idgroup];
                $isexist = $this->crud_model->moreWhere('rule',$where)->num_rows();
                if ($isexist > 0) { $ischecked = "checked='checked'"; } else { $ischecked = ''; } ?>

          <tr>
            <td class="bg-gray color-palette" width="50"><?= $no ?></td>
            <td class="bg-gray color-palette"><?= $key->name ?></td>
            <td class="bg-gray color-palette" width="50">
              <input type="checkbox" name="menu[]" value="<?= $key->id ?>" <?= $ischecked ?>>
            </td>
          </tr>
          <?php $childmenu = $this->crud_model->getDetail('menu','parent',$key->id)->result();
          foreach ($childmenu as $val) { ?>
            <!-- check if rule was exist -->
            <?php $where = ['menu' => $val->id, 'group' => $idgroup];
                  $isexist2 = $this->crud_model->moreWhere('rule',$where)->num_rows();
                  if ($isexist2 > 0) { $ischecked2 = "checked='checked'"; } else { $ischecked2 = ''; } ?>
             <tr>
               <td>-</td>
               <td><?= $val->name ?></td>
               <td>
                 <input type="checkbox" name="menu[]" value="<?= $val->id ?>" <?= $ischecked2 ?>>
               </td>
             </tr>
           <?php } ?>
        <?php $no++; endforeach; ?>
      </table>
    </div>
    <input type="hidden" id="idgroup" name="idgroup" value="<?= $idgroup ?>">
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
    <button type="button" onclick="store()" class="btn bg-purple" id="btnsubmit">Save</button>
  </div>
</form>