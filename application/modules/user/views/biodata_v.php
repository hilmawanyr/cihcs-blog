<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?= asset('lte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"') ?>">
<script type="text/javascript" src="<?= asset('custom/bio.js') ?>"></script>

<style type="text/css">
  .image {
      overflow: hidden;
      position: relative;
  }

  .labels {
      background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
      bottom: -50px;
      color: #fff;
      left: 0;
      margin: 0 auto;
      position: absolute;
      right: 0;
      text-align: center;
      transition:0.1s all;
  }

  .image:hover .labels {
    bottom: 0px; 
  }
</style>

<section class="content-header">
  <h1>
    <?php $user = $this->session->userdata('auth'); ?>
    <?= $user['name'] ?>'s Profile
  </h1>
</section>

<!-- Main content -->
<section class="content">

  <div class="row">
    <div class="col-md-3">

      <!-- Profile Image -->
      <div class="box box-primary">
        <div class="box-body box-profile">
          <div class="image">
          <img class="profile-user-img img-responsive img-circle" src="<?= base_url().substr($user['ava'], 2) ?>">
          <button class="btn bg-purple labels" onclick="getFile()">Change photo</button> 
          <div style='height: 0px;width: 0px; overflow:hidden;'>
            <input id="upfile" type="file" value="upload" onchange="sub(this)"/>
          </div> 
          </div>
          

          <h3 class="profile-username text-center"><?= $user['name'] ?></h3>
          <p class="text-muted text-center"><?= groupName($user['group']) ?></p>
          
          <hr>

          <strong><i class="fa fa-book margin-r-5"></i> Education</strong>
          <ul>
            <li id="prm"></li>
            <li id="jnr"></li>
            <li id="hgh"></li>
          </ul>

          <hr>

          <strong><i class="fa fa-map-marker margin-r-5"></i> Current Address</strong>
          <p class="text-muted" id="addr"></p>

          <hr>

          <strong><i class="fa fa-home margin-r-5"></i> Hometown</strong>
          <p class="text-muted" id="home"></p>

          <hr>

          <strong><i class="fa fa-pencil margin-r-5"></i> Interest</strong>
          <p class="text-muted" id="itr"></p>

          <hr>

          <strong><i class="fa fa-file-text-o margin-r-5"></i> About</strong>
          <p id="bout"></p>
          
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
    <div class="col-md-9">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#activity" data-toggle="tab">Recent article</a></li>
          <li><a href="#settings" data-toggle="tab" onclick="loadBio(<?= $user['uid'] ?>)">Settings</a></li>
        </ul>
        <div class="tab-content">
          <div class="active tab-pane" id="activity">
            <!-- Post -->
            <?php foreach ($article as $articles) { ?>
              <div class="post">
                <div class="user-block">
                  <img class="img-circle img-bordered-sm" src="<?= userAva($articles->created_by) ?>" alt="user image">
                  <span class="username">
                    <a href="#read" data-toggle="modal" onclick="read(<?= $articles->id ?>)">
                      <?= $articles->title ?>
                    </a>
                  </span>
                  <span class="description">
                    <?= ucfirst(getName($articles->created_by)).' - '.date('M d, Y / h.i a', strtotime($articles->created_at)) ?>
                  </span>
                </div>
                <!-- /.user-block -->
                <p>
                  <?= substr($articles->synopsys, 0,100) ?>...
                </p>
                <ul class="list-inline">
                  <li>
                    <a href="#read" class="link-black text-sm" data-toggle="modal" onclick="read(<?= $articles->id ?>)">
                      <i class="fa fa-eye margin-r-5"></i> Read more
                    </a>
                  </li>
                </ul>
              </div>  
            <?php } ?>
            
          </div>
          <!-- /.tab-pane -->
          

          <div class="tab-pane" id="settings">
            <form class="form-horizontal" id="bioform">
              <input type="hidden" id="uid" name="uid">
              <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10">
                  <input type="email" class="form-control" id="name" name="name" readonly="">
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-sm-2 control-label">E-mail</label>
                <div class="col-sm-10">
                  <input type="email" class="form-control" id="email" name="email" readonly="">
                </div>
              </div>
              <div class="form-group">
                <label for="address" class="col-sm-2 control-label">Current Address</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="address" name="address" placeholder="Address">
                </div>
              </div>
              <div class="form-group">
                <label for="hometown" class="col-sm-2 control-label">Hometown</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="hometown" name="hometown" placeholder="Hometown">
                </div>
              </div>
              <div class="form-group">
                <label for="birth" class="col-sm-2 control-label">Birthday</label>
                <div class="col-sm-10">
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="birth" name="birth">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="primary" class="col-sm-2 control-label">Primary School</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="primary" name="primary" placeholder="Primary school">
                </div>
              </div>
              <div class="form-group">
                <label for="junior" class="col-sm-2 control-label">Junior High School</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="junior" name="junior" placeholder="Junior High School">
                </div>
              </div>
              <div class="form-group">
                <label for="high" class="col-sm-2 control-label">High School</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="high" name="high" placeholder="High School">
                </div>
              </div>
              <div class="form-group">
                <label for="department" class="col-sm-2 control-label">Department</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="department" name="department" placeholder="Department in university">
                </div>
              </div>
              <div class="form-group">
                <label for="phone" class="col-sm-2 control-label">Phone</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone number">
                </div>
              </div>
              <div class="form-group">
                <label for="about" class="col-sm-2 control-label">About</label>
                <div class="col-sm-10">
                  <textarea class="form-control" id="about" name="about" placeholder="Tell about you here"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label for="interest" class="col-sm-2 control-label">Interest</label>
                <div class="col-sm-10">
                  <input type="text" 
                        class="form-control" 
                        id="interest"  
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Please type your interest(s) and separate them with commas without space"
                        name="interest"
                        placeholder="Ex. Science, Sport, Movie, Programming, etc.">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="button" onclick="savebio()" class="btn btn-danger">Save</button>
                </div>
              </div>
            </form>
          </div>
          <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
      </div>
      <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

</section>
<!-- /.content -->

<!-- modal -->
<div class="modal fade" id="read">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="artc-title"></h4>
      </div>
      <div class="modal-body">
        <b><i><small id="detail"></small></i></b>
        <hr>
        <div id="content-here"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn bg-purple" data-dismiss="modal">Close</button>
      </div>
    </div>
   </div>
</div>

<!-- bootstrap datepicker -->
<script src="<?= asset('lte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') ?>"></script>
<script type="text/javascript">
  //Date picker
  $('#birth').datepicker({
    autoclose: true
  })
</script>