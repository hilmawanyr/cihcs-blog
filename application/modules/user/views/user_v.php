<script type="text/javascript" src="<?= asset('custom/user.js') ?>"></script>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    User
    <small>manage user here</small>
  </h1>
</section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">User list</h3>
    </div>
    <div class="box-body">
      <a class="btn btn-flat btn-success" href="#addmodal" data-toggle="modal">
        <i class="fa fa-plus"></i> Add User
      </a>
      <hr>
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>No</th>
            <th>Name</th>
            <th>E-mail</th>
            <th>User ID</th>
            <th>Group</th>
            <th>Level</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1; foreach ($user as $key) { ?>
          <tr>
            <td width="80"><?= $no ?></td>
            <td><?= $key->name ?></td>
            <td><?= $key->username ?></td>
            <td><?= $key->uid ?></td>
            <td><?= groupName($key->group) ?></td>
            <td><?= levelName($key->level) ?></td>
            <td><?= userStatus($key->is_active) ?></td>
            <td width="100">
              <!-- condition for action button based on user status -->
              <!-- if user is active -->
              <?php if ($key->is_active == 1) { ?>
               
                <button onclick="edit(<?= $key->id ?>)" title="edit" class="btn btn-flat btn-info">
                  <i class="fa fa-pencil"></i>
                </button>
                <button onclick="suspend(<?= $key->uid ?>)" title="remove" class="btn btn-flat btn-danger">
                  <i class="fa fa-power-off"></i>
                </button>

              <!-- if user inactive -->
              <?php } else {  ?>

                <button onclick="reactivate(<?= $key->uid ?>)" title="activate user" class="btn btn-flat btn-success">
                  <i class="fa fa-toggle-on"></i>
                </button>
              
              <?php } ?>
              <!-- end condition  -->
            </td>
          </tr>
          <?php $no++; } ?>
        </tbody>
        <tfoot>
          <tr>
            <th>No</th>
            <th>Name</th>
            <th>E-mail</th>
            <th>User ID</th>
            <th>Group</th>
            <th>Level</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
    <div class="box-footer"></div>
    <!-- /.box-footer-->
  </div>
  <!-- /.box -->
</section>
<!-- /.content -->

<div class="modal fade" id="addmodal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-titl">Add user</h4>
      </div>
      <form id="adduser">
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <label for="title">Name</label>
              <input type="text" class="form-control" value="" id="name" name="name">
            </div>
            <div class="form-group">
              <label for="title">E-mail</label>
              <input type="text" class="form-control" value="" id="mail" name="mail">
            </div>
            <input type="hidden" name="uid" id="uid" value="">
            <div class="form-group">
              <label for="title">Group</label>
              <select name="group" class="form-control" id="groups">
                <option selected="" disabled=""></option>
                <?php foreach ($group as $key) { ?>
                  <option value="<?= $key->id ?>"><?= $key->name ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label for="title">Level</label>
              <select name="level" class="form-control" id="levels">
                <option selected="" disabled=""></option>
                <?php foreach ($level as $val) { ?>
                  <option value="<?= $val->id ?>"><?= $val->name ?></option>
                <?php } ?>
              </select>
            </div>
            <input type="hidden" name="typesubmit" id="typesubmit" value="save">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="button" onclick="create()" class="btn btn-primary" id="btnsubmit">Save</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->