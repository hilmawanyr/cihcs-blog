<script type="text/javascript" src="<?= asset('custom/level.js') ?>"></script>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Level
    <small>manage user level here</small>
  </h1>
</section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Level list</h3>
    </div>
    <div class="box-body">
      <a class="btn btn-flat btn-success" href="#addmodal" data-toggle="modal">
        <i class="fa fa-plus"></i> Add level
      </a>
      <hr>
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>No</th>
            <th>Name</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1; foreach ($level as $key) { ?>
          <tr>
            <td width="80"><?= $no ?></td>
            <td><?= $key->name ?></td>
            <td width="100">
                <button onclick="edit(<?= $key->id ?>)" title="edit" class="btn btn-flat btn-info">
                  <i class="fa fa-pencil"></i>
                </button>
                <button onclick="remove(<?= $key->id ?>)" title="remove" class="btn btn-flat btn-danger">
                  <i class="fa fa-trash"></i>
                </button>
            </td>
          </tr>
          <?php $no++; } ?>
        </tbody>
        <tfoot>
          <tr>
            <th>No</th>
            <th>Name</th>
            <th>Action</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
    <div class="box-footer"></div>
    <!-- /.box-footer-->
  </div>
  <!-- /.box -->
</section>
<!-- /.content -->

<div class="modal fade" id="addmodal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-titl">Add level</h4>
      </div>
      <form id="addlevel">
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <label for="title">Name</label>
              <input type="text" class="form-control" value="" id="name" name="name">
            </div>
            <input type="hidden" name="levelid" value="" id="levelid">
            <input type="hidden" name="formtype" id="formtype" value="save">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="button" onclick="create()" class="btn btn-primary" id="btnsubmit">Save</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->