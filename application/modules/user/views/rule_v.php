<script type="text/javascript" src="<?= asset('custom/rule.js') ?>"></script>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Rule access
    <small>manage rule access menu here</small>
  </h1>
</section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Group list</h3>
    </div>
    <div class="box-body">
      
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>No</th>
            <th>Group</th>
            <th>Add rule</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1; foreach ($group as $key) { ?>
          <tr>
            <td width="80"><?= $no ?></td>
            <td><?= $key->name ?></td>
            <td width="80" align="center">
              <button onclick="loadRule(<?= $key->id ?>)" class="btn btn-flat bg-purple" data-toggle="modal" data-target="#addmodal" title="add rule <?= $key->name ?>">
                <i class="fa fa-pencil"></i>
              </button>
            </td>
          </tr>
          <?php $no++; } ?>
        </tbody>
        <tfoot>
          <tr>
            <th>No</th>
            <th>Name</th>
            <th>Action</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
    <div class="box-footer"></div>
    <!-- /.box-footer-->
  </div>
  <!-- /.box -->
</section>
<!-- /.content -->

<div class="modal fade" id="addmodal">
  <div class="modal-dialog">
    <div class="modal-content" id="addcontent">
      <!-- form add rule show here -->
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->