<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	function detailUser() : array
	{
		$this->db->select('a.id,a.uid,a.name,a.phone,b.username,b.group,b.level,b.is_active');
		$this->db->from('user a');
		$this->db->join('login b', 'a.uid = b.uid');
		return $this->db->get()->result();
	}

	function getUser($id) : object
	{
		$this->db->select('a.id,a.uid,a.name,a.phone,b.username,b.uid,b.group,b.level,b.is_active');
		$this->db->from('user a');
		$this->db->join('login b', 'a.uid = b.uid');
		$this->db->where('a.id', $id);
		return $this->db->get()->row();
	}

}

/* End of file User_model.php */
/* Location: ./application/modules/user/models/User_model.php */