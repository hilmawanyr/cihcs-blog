<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rule_model extends CI_Model {

	function loadParent() : array
	{
		$this->db->where('location', 1);
		$this->db->where('parent', 0);
		return $this->db->get('menu')->result();
	}	

}

/* End of file Rule_model.php */
/* Location: ./application/modules/user/models/Rule_model.php */