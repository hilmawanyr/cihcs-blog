<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biodata_model extends CI_Model {

	function biodata($uid) : array
	{
		$this->db->select('*');
		$this->db->from('user a');
		$this->db->join('biodata b', 'a.uid = b.uid', 'left');
		$this->db->where('a.uid', $uid);
		return $this->db->get()->result();
	}

	function bioDetail(int $uid) : object
	{
		$this->db->select('a.name,a.uid as userid,a.email,b.*');
		$this->db->from('user a');
		$this->db->join('biodata b', 'a.uid = b.uid', 'left');
		$this->db->where('a.uid', $uid);
		return $this->db->get()->row();
	}

	function readartc($id) : object
	{
		$this->db->select('a.*, b.name as creator');
		$this->db->from('article a');
		$this->db->join('user b', 'a.created_by = b.uid');
		$this->db->where('a.id', $id);
		return $this->db->get()->row();
	}

}

/* End of file Biodata_model.php */
/* Location: ./application/modules/user/models/Biodata_model.php */