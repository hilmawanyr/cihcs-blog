<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rule extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('auth')) {
			echo "<script>alert('Please re-login to start your session!')</script>";
			redirect('/login','refresh');
			exit();
		}
		$this->load->model('Rule_model','rule');
	}

	public function index()
	{
		$data['group'] = $this->crud_model->getData('group','name','asc')->result();
		$data['page'] = "rule_v";
		$this->load->view(ADMTEMPLATE, $data);
	}

	function editrule($id)
	{
		$data['idgroup'] = $id;
		$data['menuparent'] = $this->rule->loadParent();
		$this->load->view('modaladdrule_v', $data);
	}

	function store()
	{
		$group = $this->input->post('idgroup', TRUE);
		$rules = $this->input->post('menu');

		// delete before add new rule
		$this->crud_model->rmData('rule','group',$group);

		for ($i=0; $i < count($rules); $i++) { 
			$datarule[] = [
				'menu' => $rules[$i],
				'group' => $group,
				'created_at' => date('Y-m-d H:i:s')
			];
		}

		$this->crud_model->addBatch('rule',$datarule);
	}

}

/* End of file Rule.php */
/* Location: ./application/modules/user/controllers/Rule.php */