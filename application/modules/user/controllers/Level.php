<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Level extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('auth')) {
			echo "<script>alert('Please re-login to start your session!')</script>";
			redirect('/login','refresh');
			exit();
		}
	}

	public function index()
	{
		$data['level'] = $this->crud_model->getData('level','name','asc')->result();
		$data['page'] = "level_v";
		$this->load->view(ADMTEMPLATE, $data);
	}

	function create()
	{
		$formtype = $this->input->post('formtype', TRUE);
		$name = $this->input->post('name', TRUE);

		if ($formtype == 'update') {
			$this->_update((int)$this->input->post('levelid', TRUE),(string)$name);
		} else {
			$data = [
				'name' => $name,
				'created_at' => date('Y-m-d H:i:s')
			];
			$this->crud_model->insertData('level',$data);
		}
	}

	protected function _update(int $id, string $name)
	{
		$data = [
			'name' => $name,
			'updated_at' => date('Y-m-d H:i:s')
		];
		$this->crud_model->updateData('level','id',$id,$data);
	}

	function edit($id)
	{
		$leveldata = $this->crud_model->getDetail('level','id',$id)->row();
		echo json_encode($leveldata);
	}

	function remove($id)
	{
		$this->crud_model->rmData('level','id',$id);
	}

}

/* End of file Level.php */
/* Location: ./application/modules/user/controllers/Level.php */