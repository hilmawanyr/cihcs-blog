<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biodata extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('auth')) {
			echo "<script>alert('Please re-login to start your session!')</script>";
			redirect('/login','refresh');
			exit();
		}
		$this->load->model('user/biodata_model','bio');
	}

	public function index()
	{
		$user = $this->session->userdata('auth');
		$data['article'] = $this->crud_model->getData('article','id','asc')->result();
		$data['page'] = "biodata_v";
		$this->load->view(ADMTEMPLATE, $data);
	}

	function showme()
	{
		$user = $this->session->userdata('auth');
		$mybio = $this->crud_model->getDetail('biodata','uid',$user['uid'])->row();
		echo json_encode($mybio);
	}

	function store()
	{
		extract(PopulateForm());

		$explode = explode('/', $birth);
		$birthday = $explode[2].'-'.$explode[0].'-'.$explode[1];

		$ints = explode(',', $interest);
		
		$separate = '';
		foreach ($ints as $key => $value) {
			$separate = $separate.$value.',';
		}

		$fixints = $separate;

		$bio = [
			'uid' => $uid,
			'birthday' => $birthday,
			'address' => $address,
			'hometown' => $hometown,
			'primary_school' => $primary,
			'junior_hi_school' => $junior,
			'hi_school' => $high,
			'department' => $department,
			'interest' => $fixints,
			'about' => $about,
			'phone' => $phone
		];

		$isexist = $this->crud_model->getDetail('biodata','uid',$uid)->num_rows();
		if ($isexist > 0) {
			$this->crud_model->updateData('biodata','uid',$uid,$bio);
		} else {
			$this->crud_model->insertData('biodata',$bio);
		}
		
	}

	function loadBio(int $uid)
	{
		$data = $this->bio->bioDetail($uid);
		echo json_encode($data);
	}

	function read($idartc)
	{
		$article = $this->bio->readartc($idartc);
		echo json_encode($article);
	}

}

/* End of file Biodata.php */
/* Location: ./application/modules/user/controllers/Biodata.php */