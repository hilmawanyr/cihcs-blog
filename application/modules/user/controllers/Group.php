<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('auth')) {
			echo "<script>alert('Please re-login to start your session!');</script>";
			redirect('/login','refresh');
			exit();
		}
	}

	public function index()
	{
		$data['group'] = $this->crud_model->getData('group','name','asc')->result();
		$data['page'] = "group_v";
		$this->load->view(ADMTEMPLATE, $data);
	}

	function create()
	{
		$name = $_POST['group'];
		if ($_POST['submit'] == 'save') {
			$data = [
				'name' => $name,
				'created_at' => date('Y-m-d H:i:s')
			];
			$this->crud_model->insertData('group', $data);

		} else {
			$this->update($name, $_POST['groupid']);
		}
	}

	function edit($id)
	{
		$loadgroup = $this->crud_model->getDetail('group','id',$id)->row();
		echo json_encode($loadgroup);
	}

	function update($gname,$gid)
	{
		$name = $gname;
		$id = $gid;

		$data = [
			'name' => $name,
			'updated_at' => date('Y-m-d H:i:s')
		];

		$this->crud_model->updateData('group','id',$id,$data);
	}

	function destroy($id)
	{
		$this->crud_model->rmData('group','id',$id);
	}

}

/* End of file Group.php */
/* Location: ./application/modules/user/controllers/Group.php */