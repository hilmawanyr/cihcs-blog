<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('auth')) {
			echo "<script>alert('Please re-login to start your session!')</script>";
			redirect('/login','refresh');
			exit();
		}
		$this->load->model('user/user_model','user');
	}

	public function index()
	{
		$data['group'] = $this->crud_model->getData('group','name','asc')->result();
		$data['level'] = $this->crud_model->getData('level','name','asc')->result();
		$data['user'] = $this->user->detailuser();
		$data['page'] = "user_v";
		$this->load->view(ADMTEMPLATE, $data);
	}

	function create()
	{
		$group = $_POST['group'];
		$level = $_POST['level'];

		if ($_POST['typesubmit'] == 'update') {
			$this->_update((int)$_POST['uid'],(int)$group,(int)$level);
		} else {
			$name = $_POST['name'];
			$mail = $_POST['mail'];

			$uid = $this->_makeUid();

			$datauser = [
				'uid' => $uid,
				'name' => $name,
				'mail' => $mail,
				'ava' => './assets/img/user_icon.png'
			];
			$this->crud_model->insertData('user',$datauser);

			$this->_makeLoginAccount($mail, (int)$group, (int)$level, (string)$uid);
		}
	}

	protected function _makeUid() : string
	{
		$hash 	= NULL;
        $n 		= 8;
        $char 	= "0123456789";
        for ($i = 0; $i < $n; $i++) {
	        $rand  = rand(1, strlen($char));
	        $hash .= substr($char, $rand, 1);
	    }
	    $key = $hash;
	    return $key;
	}

	protected function _makeLoginAccount(string $m, int $g, int $l, string $u)
	{
		$prefixmail = explode('@', $m);
		$pass = $this->_generatePass($prefixmail[0]);

		$datalogin = [
			'uid' => $u,
			'username' => $m,
			'password' => $pass,
			'group' => $g,
			'level' => $l,
			'is_active' => 1,
			'created_at' => date('Y-m-d H:i:s')
		];
		$this->crud_model->insertData('login',$datalogin);
	}

	protected function _generatePass(string $param) : string
	{
		return sha1(md5($param).PASSGUIDE);
	}

	function suspend(string $uid)
	{
		$this->crud_model->updateData('login','uid',$uid,['is_active' => NULL]);
	}

	function remove(int $id)
	{
		$this->crud_model->rmData('login','id',$id);	
	}

	function edit(int $id)
	{
		$detiluser = $this->user->getUser($id);
		echo json_encode($detiluser);
	}

	protected function _update(string $u, int $g, int $l)
	{
		$dataedit = [
			'group' => $g,
			'level' => $l
		];
		$this->crud_model->updateData('login','uid',$u,$dataedit);
	}

	function reactivate(string $uid)
	{
		$this->crud_model->updateData('login','uid',$uid,['is_active' => 1]);
	}
}

/* End of file User.php */
/* Location: ./application/modules/user/controllers/User.php */