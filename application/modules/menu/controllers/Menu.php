<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('auth')) {
			echo "<script>alert('Please re-login to start your session!')</script>";
			redirect('/login','refresh');
			exit();
		}
	}

	public function index()
	{
		$data['menu'] = $this->crud_model->getData('menu','name','asc')->result();
		$data['page'] = "menu_v";
		$this->load->view(ADMTEMPLATE, $data);
	}

	function notif()
	{
		$notif = $this->crud_model->getDetail('notif','status',1)->num_rows();
		echo $notif;
	}

}

/* End of file Menu.php */
/* Location: ./application/modules/menu/controllers/Menu.php */