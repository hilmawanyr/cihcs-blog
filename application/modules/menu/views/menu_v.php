<script type="text/javascript" src="<?= asset('custom/menu.js') ?>"></script>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Menu
    <small>manage menu here</small>
  </h1>
</section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Menu list</h3>
    </div>
    <div class="box-body">
      <a class="btn btn-flat btn-success" data-toggle="modal" href="#addmodal">
        <i class="fa fa-plus"></i> Add menu
      </a>
      <hr>
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>No</th>
            <th>Menu</th>
            <th>Location</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody id="showlist">
          <?php $no = 1; foreach ($data as $key) { ?>
          <tr>
            <td><?= $no ?></td>
            <td><?= $key->name ?></td>
            <td><?= $key->created_at ?></td>
            <td>
              <button class="btn btn-flat bg-purple" onclick="beforeEdit(<?= $key->id ?>)">
                <i class="fa fa-pencil"></i>
              </button>
              <button class="btn btn-flat btn-danger" onclick="remove(<?= $key->id ?>)">
                <i class="fa fa-trash"></i>
              </button>
            </td>
          </tr>
        <?php $no++; } ?>
        </tbody>
        <tfoot>
          <tr>
            <th>No</th>
            <th>Menu</th>
            <th>Location</th>
            <th>Action</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
    <div class="box-footer"></div>
    <!-- /.box-footer-->
  </div>
  <!-- /.box -->
</section>
<!-- /.content -->

<div class="modal fade" id="addmodal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-titl">Add category</h4>
      </div>
      <form id="addcategory">
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Category name</label>
              <input type="text" class="form-control" id="namecat" name="name" id="exampleInputEmail1" placeholder="Enter category name">
              <input type="hidden" value="" name="idcat" id="idcat">
              <input type="hidden" name="saveup" value="save" id="saveup">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="savecategory()" id="btnsubmit">Save</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->