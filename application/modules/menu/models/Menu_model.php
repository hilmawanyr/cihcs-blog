<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends CI_Model {

	function getMenu($type,$group) : array
	{
		$this->db->distinct();
		$this->db->select('a.id,a.name,a.url,a.icon,b.group');
		$this->db->from('menu a');
		$this->db->join('rule b', 'a.id = b.menu');
		$this->db->where('a.location', 1);
		$this->db->where('a.parent', $type);
		$this->db->where('b.group', $group);
		$this->db->order_by('a.name', 'desc');
		return $this->db->get()->result();
	}	

}

/* End of file Menu_model.php */
/* Location: ./application/modules/menu/models/Menu_model.php */