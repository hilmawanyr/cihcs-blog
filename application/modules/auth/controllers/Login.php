<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('auth/login_model','login');
	}

	public function index()
	{
		if ($this->session->userdata('auth')) {
			redirect('home','refresh');
		} else {
			$this->load->view('login_v');
		}
	}

	function loginAttemp()
	{
		$u = $this->input->post('user', TRUE);
		$p = $this->input->post('pass', TRUE);

		$check = $this->login->isAvailable((string) $u, (string) $p);

		if ((bool) $check) {
			// check for account activation
			foreach ($check as $key) {
				// if account is active
				if ($key->is_active == 1) {
					$this->_makeLoginSession($check);
				} else {
					echo "<script>alert('Your account has been suspend!');history.go(-1);</script>";
				}
			}
			
		} else {
			echo "<script>alert('Password do not match or your account is unavailable!');history.go(-1);</script>";
		}
	}

	protected function _makeLoginSession(array $useraccount)
	{
		foreach ($useraccount as $key) {
			$loaduser = $this->login->loadDataUser($key->uid);
			$this->_loginSuccess($loaduser);
		}
	}

	protected function _loginSuccess(array $datauser) 
	{
		foreach ($datauser as $key) {
			$session = array(
				'uid' => $key->uid,
				'username' => $key->username,
				'group' => $key->group,
				'level' => $key->level,
				'name' => $key->name,
				'ava' => $key->avatar
			);
			
			$this->session->set_userdata('auth', $session);
		}
		redirect('home','refresh');
	}

	function destroy()
	{
		$this->session->sess_destroy();
		redirect('login','refresh');
	}

}

/* End of file Login.php */
/* Location: ./application/modules/auth/controllers/Login.php */