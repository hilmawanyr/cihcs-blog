<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

	function isAvailable($u, $p) : array
	{
		$pass = sha1(md5($p).PASSGUIDE);
		$this->db->where('username', $u);
		$this->db->where('password', $pass);
		return $this->db->get('login', 1)->result();
	}

	function loadDataUser($uid) : array
	{
		$this->db->select('a.uid,a.username,a.group,a.level,b.name,b.avatar');
		$this->db->from('login a');
		$this->db->join('user b', 'a.uid = b.uid');
		$this->db->where('a.uid', $uid);
		return $this->db->get()->result();
	}

}

/* End of file Login_model.php */
/* Location: ./application/modules/auth/models/Login_model.php */