var base_url = window.location.origin;

// load bio detail
$(window).on('load', function(){
	$.get(base_url+'/bio/itsme', function(res){
		var parse = JSON.parse(res);
		$('#prm').text(parse.primary_school);
		$('#jnr').text(parse.junior_hi_school);
		$('#hgh').text(parse.hi_school);
		$('#addr').text(parse.address);
		$('#home').text(parse.hometown);
		$('#bout').text(parse.about);

		var interest = parse.interest.split(",");
		var lable = ["","info","warning","success","primary","danger"];
		$.each(interest, function(i, value){
			var x = Math.floor((Math.random() * 5) + 1);
			$('#itr').append('<span class="label label-'+lable[x]+'">'+value+'</span> ');
		});
	})
})

function loadBio(uid) {
	$.get(base_url+'/bio/'+uid+'/user', function(respon) {
		var bio = JSON.parse(respon);
		$('#name').val(bio.name);
		$('#email').val(bio.email);
		$('#uid').val(bio.userid);
		$('#address').val(bio.address);
		$('#hometown').val(bio.hometown);
		$('#birth').val(bio.birthday);
		$('#primary').val(bio.primary_school);
		$('#junior').val(bio.junior_hi_school);
		$('#high').val(bio.hi_school);
		$('#department').val(bio.department);
		$('#phone').val(bio.phone);
		$('#about').val(bio.about);
		$('#interest').val(bio.interest);
	})
}

function savebio() {
	$.ajax({
		url: base_url+'/bio/store',
		type: 'POST',
		data: $('#bioform').serialize(),
		success: function() {
			swal("Good Job!", "Bio has been saved!", "success")
			.then(function() {
				location.reload();
			})
		}
	})
}

function read(id) {
	$.get(base_url+'/bio/'+id+'/readarticle', function(response){
		var article = JSON.parse(response);
		$('#artc-title').text(article.title);
		$('#detail').text('by '+article.creator+', '+article.created_at);
		$('#content-here').empty();
		$('#content-here').append(article.content);
	})
}

function getFile(){
   document.getElementById("upfile").click();
 }