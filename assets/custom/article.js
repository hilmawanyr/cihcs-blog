var base_url = window.location.origin;

// load table of article list
$.get(base_url+'/article/getdata', function (response) {
	$('#showlist').html(response);
});

// post article
function post() {
	$.ajax({
		url: base_url+'/article/post',
		type: 'POST',
		data: $('#formeditor').serialize(),
		success: function() {
			swal("Good job!", "Article has been publish!", "success")
			.then(()=>{
				location.reload();
			});
		}
	});
}

// look article detail
function detail(id) {
	$.post(base_url+'/article/'+id+'/detail', function(response) {
		$('#detail').modal('show');
		var article = JSON.parse(response);
		$('#title').val(article.title);
		$('#cat').val(article.catname);
		$('#syn').val(article.synopsys);
		$('#content').append(article.content);
	});
}

// update article
function update(id) {
	$.ajax({
		url: base_url+'/article/'+id+'/update',
		type: 'POST',
		data: $('#formupdate').serialize(),
		success: function() {
			swal("Good job!", "Article has been updated!", "success")
			.then(()=>{
				window.location.href=base_url+"/article/list";
			});
		}
	});
}

// remove article
function remove(id) {
	swal({
	  	title: "Are you sure?",
	  	text: "Once deleted, you will not be able to recover this article!",
	  	icon: "warning",
	  	buttons: true,
	  	dangerMode: true,
	})
	.then((willDelete) => {
	  	if (willDelete) {
	  		$.post(base_url+'/article/'+id+'/destroy', function(){
	  			swal("Poof! article has been deleted!", {
			      	icon: "success",
			    })
			    .then(()=> {
			    	location.reload();
			    });
			    
	  		});
	  	} else {
	    	swal("Your article is safe!");
	  	}
	});
}

function publish(id,status) {
	if (status === 1) {
		var fortitle = 'Are you sure to publish?';
		var message = 'article has been publish!';
		var final = 'Article'
	} else {
		var fortitle = 'Do you think this article must be recheck?';
		var message = 'article will be recheck!';
		var final = 'Article'
	}

	swal({
	  	title: fortitle,
	  	icon: "warning",
	  	buttons: true,
	  	dangerMode: true,
	})
	.then((willPublish) => {
	  	if (willPublish) {
	  		$.post(base_url+'/article/'+id+'/publish/'+status, function(){
	  			swal("Poof! "+message, {
			      	icon: "success",
			    })
			    .then(()=> {
			    	location.reload();
			    });
			    
	  		});
	  	} else {
	    	swal("Your article is safe!");
	  	}
	});
}