var base_url = window.location.origin;

// create group
function create() {
	$.ajax({
		url: base_url+'/user/group/create',
		type: 'POST',
		data: $('#addgroup').serialize(),
		success: function () {
			var typeSubmit = $('#submit').val();
			if (typeSubmit === 'save') {
				var textalert = 'created';
			} else {
				var textalert = 'updated';
			}
			swal("Good job!", "Group has been "+textalert+"!", "success")
			.then(()=>{
				location.reload();
			});
		}
	});
}

// edit group
function edit(id) {
	$.get(base_url+'/group/edit/'+id, function(res){
		$('#addmodal').modal('show');
		$('#modal-titl').text('Edit group');
		$('#btnsubmit').text('Update');
		var group = JSON.parse(res);
		$('#name').val(group.name);
		$('#groupid').val(group.id);
		$('#submit').val('update');
	});
}

// handle add form on modal edit close when there's no change
$(document).ready(function(){
	$('#addmodal').on('hidden.bs.modal', function(){
		$('#modal-titl').text('Add Group');
		$('#btnsubmit').text('Save');
		$('#name').val('');
		$('#groupid').val('');
		$('#submit').val('save');
	});
});

// remove group
function remove(id) {
	swal({
	  	title: "Are you sure?",
	  	text: "Once deleted, you will not be able to recover this group!",
	  	icon: "warning",
	  	buttons: true,
	  	dangerMode: true,
	})
	.then((willDelete) => {
	  	if (willDelete) {
	  		$.post(base_url+'/group/'+id+'/destroy', function(){
	  			swal("Poof! group has been deleted!", {
			      	icon: "success",
			    })
			    .then(()=> {
			    	location.reload();
			    });
			    
	  		});
	  	} else {
	    	swal("Your group is safe!");
	  	}
	});
}