var base_url = window.location.origin;

// repo needed
function detail(id,search) {
	$.get(base_url+'/'+search+'/'+id+'/detail', function(res){
		$('#detailmodal').modal('show');
		var file = JSON.parse(res);
		var type = file.name.split(".");
		$('#filename').val(file.name);
		$('#filetype').val(type[1]);
		$('#filesize').val(file.size);
		$('#datecreate').val(file.created_at);
	})	
}

function setstatic(id, prt, name) {
	if (prt <= 10 && id != 18) {
		$('#about').modal('show');
		$('#idabout').val(id);
		$('#modtitle').text(name);
	} else if (prt <= 10 && id == 18) {
		$('#organize').modal('show');
		$('#idorganize').val(id);
		$('#modstitle').text(name);
	} else if (prt == 14) {
		$('#contact').modal('show');
		$('#idcontact').val(id);
		$('#contactitle').text(name);
		$('#for-lable').text(name);
	}
}

function storeabout() {
	$.ajax({
		url: base_url+'/about/store',
		type: 'POST',
		data: $('#about-form').serialize(),
		success: function() {
			swal("Good job!","Content successfully added!","success")
			.then(function(){
				location.reload();
			})
		}
	})
}

function storecontact() {
	$.ajax({
		url: base_url+'/contact/store',
		type: 'POST',
		data: $('#contact-form').serialize(),
		success: function() {
			swal("Good job!","Content successfully added!","success")
			.then(function(){
				location.reload();
			})
		}
	})
}

function editstatic(id,parent,name) {
	if (parent <= 10 && id == 18) {
		$('#organize').modal('show');
		$('#idorganize').val(id);
		$('#modstitle').text('Change '+name);
	} else if (parent <= 10 && id != 18) {
		$('#about').modal('show');
		$('#idabout').val(id);
		$('#modtitle').text('Change '+name);
		$('#saveabout').text('Update');
		$.get(base_url+'/edit/landing/'+id, function(res){
			var parse = JSON.parse(res);
			$('#here').empty();
			$('#here').append(parse.content);
		});
	} else if (parent == 14) {
		$('#contact').modal('show');
		$('#idcontact').val(id);
		$('#contactitle').text('Change '+name);
		$('#for-lable').text(name);
		$('#savecontact').text('Update');
		$.get(base_url+'/edit/landing/'+id, function(res){
			var parse = JSON.parse(res)
			$('#input-contact').val(parse.content);
		});
	}
}