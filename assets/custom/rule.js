var base_url = window.location.origin;

// load modal for add rule
function loadRule (id) {
	$('#addcontent').load(base_url+'/rule/'+id+'/add');
}

// add rule
function store() {
	$.ajax({
		url: base_url+'/rule/store',
		type: 'POST',
		data: $('#addrules').serialize(),
		success: function() {
			swal("Good job!", "Rule(s) successfully saved!", "success")
			.then(function(){
				location.reload();
			})
		}
	})
}