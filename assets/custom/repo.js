var base_url = window.location.origin;

// create repo
function create() {
	$.ajax({
		url: base_url+'/repo/create',
		type: 'POST',
		data: $('#addrepo').serialize(),
		success: function() {
			swal("Good job!", "Repository has been create!", "success")
			.then(()=>{
				location.reload();
			});
		}
	})
}

// rename repo
function editRepo(id) {
	$.get(base_url+'/repo/edit/'+id, function(res){
		var repo = JSON.parse(res);
		$('#reponame').val(repo.name);
		$('#repoid').val(repo.id);

		if (repo.is_private === '1') {
			$('#repotype').prop('checked', true);	
		} else {
			$('#repotype').prop('checked', false);
		}
		
	});
}

// update repo
function updateRepo() {
	$.ajax({
		url: base_url+'/repos/change',
		type: 'POST',
		data: $('#editrepoform').serialize(),
		success: function() {
			swal("Good job!", "Repository has been change!", "success")
			.then(()=>{
				location.reload();
			});
		}
	});
}

// remove repo
function rmRepo(id) {
	swal({
	  	title: "Are you sure to delete repository and its content(s) inside?",
	  	text: "Once deleted, you will not be able to recover this repository!",
	  	icon: "warning",
	  	buttons: true,
	  	dangerMode: true,
	})
	.then((willDelete) => {
	  	if (willDelete) {
	  		$.post(base_url+'/repos/'+id+'/remove/', function(){
	  			swal("Poof! repo has been deleted!", {
			      	icon: "success",
			    })
			    .then(()=> {
			    	location.href=base_url+'/repo/list';
			    });
			    
	  		});
	  	} else {
	    	swal("Your repository is safe!");
	  	}
	});
}

// submit file
function mySubmit() {
	swal("Good job!", "File upload successfully!", "success")
	.then(()=>{
		$("#maincontent").load(location.href+" #maincontent>*");
	});
}

// change filetype
function changeFileType(id,type) {
	$.ajax({
		url: base_url+'/file/'+id+'/type/'+type,
		type: 'POST',
		success: function() {
			swal("Good job!", "File type has been change!", "success")
			.then(()=>{
				$("#iconfile-"+id).load(location.href + " #iconfile-"+id);
				$("#list-"+id).empty();
				$("#list-"+id).load(location.href+" #list-"+id+">*");
			});
		}
	});
}

// remove file
function rmFile(id) {
	swal({
	  	title: "Are you sure?",
	  	text: "Once deleted, you will not be able to recover this file!",
	  	icon: "warning",
	  	buttons: true,
	  	dangerMode: true,
	})
	.then((willDelete) => {
	  	if (willDelete) {
	  		$.post(base_url+'/file/destroy/'+id, function(){
	  			swal("Poof! file has been deleted!", {
			      	icon: "success",
			    })
			    .then(()=> {
			    	location.reload();
			    });
			    
	  		});
	  	} else {
	    	swal("Your file is safe!");
	  	}
	});
}

function detail(id) {
	$.get(base_url+'/file/'+id+'/detail', function(res){
		var file = JSON.parse(res);
		var type = file.name.split(".");
		$('#filename').val(file.name);
		$('#filetype').val(type[1]);
		$('#filesize').val(file.size);
		$('#datecreate').val(file.created_at);
	})
}