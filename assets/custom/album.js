var base_url = window.location.origin;

// create album
function create() {
	$.ajax({
		url: base_url+'/album/create',
		type: 'POST',
		data: $('#addalbum').serialize(),
		success: function() {
			swal("Good job!", "Album has been create!", "success")
			.then(()=>{
				location.reload();
			});
		}
	})
}

// edit album
function editAlbum(id) {
	$.get(base_url+'/album/edit/'+id, function(res){
		var repo = JSON.parse(res);
		$('#albumname').val(repo.name);
		$('#albumid').val(repo.id);

		if (repo.is_private === '1') {
			$('#albumtype').prop('checked', true);	
		} else {
			$('#albumtype').prop('checked', false);
		}
		
	});
}

// update album
function updateAlbum() {
	$.ajax({
		url: base_url+'/albums/change',
		type: 'POST',
		data: $('#editalbumform').serialize(),
		success: function() {
			swal("Good job!", "Album has been change!", "success")
			.then(()=>{
				location.reload();
			});
		}
	});
}

// remove album
function rmAlbum(id) {
	swal({
	  	title: "Are you sure to delete album and its content(s)?",
	  	text: "Once deleted, you will not be able to recover this album!",
	  	icon: "warning",
	  	buttons: true,
	  	dangerMode: true,
	})
	.then((willDelete) => {
	  	if (willDelete) {
	  		$.post(base_url+'/albums/'+id+'/remove/', function(){
	  			swal("Poof! Album has been deleted!", {
			      	icon: "success",
			    })
			    .then(()=> {
			    	location.href=base_url+'/album';
			    });
			    
	  		});
	  	} else {
	    	swal("Your album is safe!");
	  	}
	});
}

// change filetype
function changeFileType(id,type) {
	$.ajax({
		url: base_url+'/photo/'+id+'/type/'+type,
		type: 'POST',
		success: function() {
			swal("Good job!", "Privation type has been change!", "success")
			.then(()=>{
				$("#iconfile-"+id).load(location.href + " #iconfile-"+id);
				$("#list-"+id).empty();
				$("#list-"+id).load(location.href+" #list-"+id+">*");
			});
		}
	});
}

// detail photo
function detail(id) {
	$.get(base_url+'/photo/'+id+'/detail', function(res){
		var file = JSON.parse(res);
		var type = file.name.split(".");
		$('#filename').val(file.name);
		$('#filetype').val(type[1]);
		$('#filesize').val(file.size);
		$('#datecreate').val(file.created_at);
	})
}

// remove photo
function rmPhoto(id) {
	swal({
	  	title: "Are you sure?",
	  	text: "Once deleted, you will not be able to recover this photo!",
	  	icon: "warning",
	  	buttons: true,
	  	dangerMode: true,
	})
	.then((willDelete) => {
	  	if (willDelete) {
	  		$.post(base_url+'/photo/destroy/'+id, function(){
	  			swal("Poof! file has been deleted!", {
			      	icon: "success",
			    })
			    .then(()=> {
			    	location.reload();
			    });
			    
	  		});
	  	} else {
	    	swal("Your photo is safe!");
	  	}
	});
}