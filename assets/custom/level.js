var base_url = window.location.origin;

function create() {
	$.ajax({
		url: base_url+'/level/create',
		type: 'POST',
		data: $('#addlevel').serialize(),
		success: function() {
			var formtyp = $('#formtype').val();
			if (formtyp === 'save') {
				argument = "created";
			} else {
				argument = "updated";
			}
			swal("Good job!", "Level has been successfully "+argument+"!", "success")
			.then(function(){
				location.reload();
			});
		}
	});
}

function edit(id) {
	$.get(base_url+'/level/'+id+'/edit', function(respons){
		$('#addmodal').modal('show');
		$('#modal-titl').text('Edit data');
		$('#formtype').val('update');
		$('#btnsubmit').text('Update');
		var parserespon = JSON.parse(respons);
		$('#name').val(parserespon.name);
		$('#levelid').val(parserespon.id);
	});
}

// handle add form on modal edit close when there's no change
$(document).ready(function(){
	$('#addmodal').on('hidden.bs.modal', function(){
		$('#modal-titl').text('Add data');
		$('#formtype').val('save');
		$('#btnsubmit').text('Save');
		$('#name').val('');
		$('#levelid').val('');
	});
});

function remove(id) {
	swal({
	  	title: "Are you sure to delete level?",
	  	text: "Once deleted, you will not be able to recover this level!",
	  	icon: "warning",
	  	buttons: true,
	  	dangerMode: true,
	})
	.then((willDelete) => {
	  	if (willDelete) {
	  		$.post(base_url+'/level/'+id+'/remove/', function(){
	  			swal("Poof! level has been deleted!", {
			      	icon: "success",
			    })
			    .then(()=> {
			    	location.reload();
			    });
			    
	  		});
	  	} else {
	    	swal("Your level is safe!");
	  	}
	});
}