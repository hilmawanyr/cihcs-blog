var base_url = window.location.origin;

// insert category with ajax
function savecategory() {
	$.ajax({
		url: base_url+'/category/store',
		type: 'POST',
		data: $('#addcategory').serialize(),
		success: function() {
			swal("Good job!", "Category saved successfully!", "success")
			.then(()=>{
				$('#addmodal').modal('hide');
				location.reload();
				// $("#showlist").load(location.href + " #showlist");
			});
		}
	});
}

// load modal for edit category
function beforeEdit(id) {
	$('#addmodal').modal('show');
	$.get(base_url+'/category/loadedit/'+id, (response) => {
		var parseRespon = JSON.parse(response);
		$('#namecat').val(parseRespon.name);
		$('#idcat').val(parseRespon.id);
		$('#saveup').val('update');
		$('#modal-titl').text('Edit Category');
		$('#btnsubmit').text('Update');
	});
}

// handle add form on modal edit close when there's no change
$(document).ready(function(){
	$('#addmodal').on('hidden.bs.modal', function(){
		$('#modal-titl').text('Add Category');
		$('#saveup').val('save');
		$('#btnsubmit').text('Save');
		$('#namecat').val('');
		$('#idcat').val('');
	});
});

// remove category
function remove(id) {
	swal({
	  	title: "Are you sure?",
	  	text: "Once deleted, you will not be able to recover this category!",
	  	icon: "warning",
	  	buttons: true,
	  	dangerMode: true,
	})
	.then((willDelete) => {
	  	if (willDelete) {
	  		$.post(base_url+'/category/destroy/'+id, function(){
	  			swal("Poof! category has been deleted!", {
			      	icon: "success",
			    })
			    .then(()=> {
			    	location.reload();
			    });
			    
	  		});
	  	} else {
	    	swal("Your category is safe!");
	  	}
	});
}