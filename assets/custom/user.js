var base_url = window.location.origin;

function create() {
	$.ajax({
		url: base_url+'/user/create',
		type: 'POST',
		data: $('#adduser').serialize(),
		success: function() {
			swal("Good job!", "User has been create successfully!", "success")
			.then(function(){
				location.reload();
			});
		}
	});
}

function edit(id) {
	$.post(base_url+'/user/'+id+'/edit', function(res){
		$('#addmodal').modal('show');
		$('#modal-titl').text('Edit user');
		$('#btnsubmit').text('Update');
		$('#name').prop("readonly", true);
		$('#mail').prop("readonly", true);
		var user = JSON.parse(res);
		$('#name').val(user.name);
		$('#mail').val(user.username);
		$('#uid').val(user.uid);
		$('#groups').val(user.group);
		$('#levels').val(user.level);
		$('#typesubmit').val('update');
	});
}

// handle add form on modal edit close when there's no change
$(document).ready(function(){
	$('#addmodal').on('hidden.bs.modal', function(){
		$('#modal-titl').text('Add data');
		$('#typesubmit').val('save');
		$('#btnsubmit').text('Save');
		$('#name').val('');
		$('#mail').val('');
		$('#groups').val('');
		$('#levels').val('');
		$('#mail').prop("readonly", false);
		$('#name').prop("readonly", false);
	});
});

function suspend(id) {
	swal({
	  	title: "Are you sure to suspend this user?",
	  	text: "You can reactivate this user by click Activate user on this page",
	  	icon: "warning",
	  	buttons: true,
	  	dangerMode: true,
	})
	.then((willDelete) => {
	  	if (willDelete) {
	  		$.post(base_url+'/user/'+id+'/suspend/', function(){
	  			swal("Poof! user has been suspended!", {
			      	icon: "success",
			    })
			    .then(()=> {
			    	location.reload();
			    });
			    
	  		});
	  	} else {
	    	swal("User is safe!");
	  	}
	});
}

function reactivate(uid) {
	swal({
	  	title: "Are you sure to reactivate this user?",
	  	icon: "warning",
	  	buttons: true,
	  	dangerMode: false,
	})
	.then((willDelete) => {
	  	if (willDelete) {
	  		$.post(base_url+'/user/'+uid+'/react', function(respon){
				swal("Good job!", "User has been reactivated!", "success")
				.then(function(){
					location.reload();
				});
			});
	  	} else {
	    	swal("User is safe!");
	  	}
	});
}